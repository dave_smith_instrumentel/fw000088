
import os
import pandas as pd
import argparse
import time
import struct
import serial

import hardware

import plotly.graph_objects as go
from plotly.subplots import make_subplots

class CRC16Stream(object):
    """
    Special object to calculate the CRC for a stream
    """

    CRC16 = 0x8005

    def __init__(self):
        self.out = 0
        self.bits_read = 0

    def reset(self):
        self.out = 0
        self.bits_read = 0

    def add_data(self, bindata):
        size = len(bindata)
        data_index = 0
        #out = 0;  # 16b
        #bits_read = 0
        bit_flag = 0

        while (size > 0):
            bit_flag = self.out >> 15;

            # Get next bit: */
            self.out <<= 1;
            self.out |= ((bindata[data_index] >> self.bits_read) & 1)  # item a) work from the least significant bits
            self.out = self.out & 0xFFFF

            # Increment bit counter: */
            self.bits_read += 1;
            if (self.bits_read > 7):
                self.bits_read = 0;
                data_index += 1;
                size -= 1;

            # Cycle check: */
            if (bit_flag):
                self.out ^= 0x8005;
                self.out &= 0xFFFF

    def end(self):
        """
        Calculate the CRC after all binary data has been fed in
        :return:
        """

        # item b) "push out" the last 16 bits
        for i in range(16):
            bit_flag = self.out >> 15;
            self.out <<= 1;
            self.out = self.out & 0xFFFF

            if (bit_flag):
                self.out ^= 0x8005;
                self.out &= 0xFFFF

        # item c) reverse the bits
        crc = 0;
        i = 0x8000;
        j = 0x0001;

        # for (; i != 0; i >>=1, j <<= 1)
        while (i != 0):

            if (i & self.out):
                crc |= j;

            i >>= 1
            j <<= 1

        return crc

class UARTEncoder(object):
    """
    Basic Uart Encoder...
    """

    # Feature Flags

    UARTENCODER_FLAGS_NONE          = 0
    UARTENCODER_FLAGS_CRCENABLED    =  0x01
    UARTENCODER_FLAGS_TWOBYTLELEN   = 0x02

    def __init__(self):
        """

        """

        self.sendcall = None
        self.tag = None

        self.feature_flags = self.UARTENCODER_FLAGS_NONE

        # States for stream
        self.is_stream = False
        self.stream_bytes_rem = 0
        self.stream_crc = CRC16Stream()

    def set_send_call(self, call):
        """
        Set the send call
        :param call:
        :return:
        """

        self.sendcall = call

    def send_data(self, bindata):
        """
        Send data to the sendcall function
        :param bindata:
        :return:
        """
        print("Sending: {}".format(bindata))

        if self.sendcall is not None:
            self.sendcall(bindata, self.tag)

    def send_packet(self, bindata):
        """
        Send binary data packet as whole defined by object settings and send to the sendcall obj
        :param bindata:
        :return:
        """
        print("send_packet")

        # Send preamble + len
        self.send_preamble_wlen(len(bindata))

        # send payload
        self.send_data(bindata)

        # Send CRC
        if self.feature_flags & self.UARTENCODER_FLAGS_CRCENABLED != 0:
            crc16 = gen_crc16(bindata)
            crc_bin = struct.pack("<H", crc16)
            self.send_data(crc_bin)


    def stream_packet_init(self, total_len):
        """
        Stream a pakcet when a broken up buffer is needed. total length is needed before hand as
        this is sent first
        :param total_len:
        :return:
        """

        self.is_stream = True

        self.stream_bytes_rem = total_len

        self.send_preamble_wlen(total_len)

        if total_len == 0:
            # Ensure we close off
            if self.feature_flags & self.UARTENCODER_FLAGS_CRCENABLED != 0:
                crc16 = gen_crc16(bytearray([]))
                crc_bin = struct.pack("<H", crc16)
                self.send_data(crc_bin)

            self.is_stream = False

        # Init the CRC
        self.stream_crc.reset()

    def stream_packet_data(self, bindata):
        """
        Transmit partial binary data when in a stream, must call stream_packet_init to start a stream of a packet
        :param bindata:
        :return:
        """

        if self.is_stream:
            return # #TODO Raise a fault here

        if self.stream_bytes_rem < len(bindata):
            # Transmitting too much data #TODO raise a fault here
            bindata = bindata[:self.stream_bytes_rem]

        # TX the data
        self.send_data(bindata)

        # Update the CRC
        self.stream_crc.add_data(bindata)

        # Decrement length remain
        self.stream_bytes_rem -= len(bindata)

        # Are we at the end?
        if self.stream_bytes_rem == 0:
            # At end, TX CRC value if needed

            if self.feature_flags & self.UARTENCODER_FLAGS_CRCENABLED != 0:
                crc16 = self.stream_crc.end()
                crc_bin = struct.pack("<H", crc16)
                self.send_data(crc_bin)

            # End the straem
            self.is_stream = False



    def send_preamble_wlen(self, msg_len):
        """
        Send the Preamble start and length,
        To be called by send_packet and streaming too...
        :param msg_len:
        :return:
        """

        # Send preamble
        self.send_data(bytearray([0xA5, 0x5A]))

        # Send the length

        if self.feature_flags & self.UARTENCODER_FLAGS_TWOBYTLELEN != 0:
            # Two byte length here
            len_bin = struct.pack("<H", msg_len)
        else:
            len_bin = struct.pack("<B", msg_len)

        self.send_data(len_bin)


def gen_crc16(data):
    size = len(data)
    data_index = 0
    out = 0;  # 16b
    bits_read = 0
    bit_flag = 0

    while (size > 0):
        bit_flag = out >> 15;

        # Get next bit: */
        out <<= 1;
        out |= ((data[data_index] >> bits_read) & 1)  # item a) work from the least significant bits
        out = out & 0xFFFF

        # Increment bit counter: */
        bits_read += 1;
        if (bits_read > 7):
            bits_read = 0;
            data_index += 1;
            size -= 1;

        # Cycle check: */
        if (bit_flag):
            out ^= 0x8005;
            out &= 0xFFFF

    # item b) "push out" the last 16 bits
    for i in range(16):
        bit_flag = out >> 15;
        out <<= 1;
        out = out & 0xFFFF

        if (bit_flag):
            out ^= 0x8005;
            out &= 0xFFFF

    # item c) reverse the bits
    crc = 0;
    i = 0x8000;
    j = 0x0001;

    # for (; i != 0; i >>=1, j <<= 1)
    while (i != 0):

        if (i & out):
            crc |= j;

        i >>= 1
        j <<= 1

    return crc

def build_message(message):
    _preamble = bytearray([0xA5, 0x5A])
    _len = struct.pack("<H", len(message))

    _data = message.encode("utf8")

    _crc = gen_crc16(message.encode("utf8"))

    _crc_bin = struct.pack("<H", _crc)

    _complete_msg = _preamble + _len + _data + _crc_bin

    # print("Sending: {}".format(_complete_msg))
    return _complete_msg


if __name__ == "__main__":

    # Parse the arguments
    parser = argparse.ArgumentParser()

    parser.add_argument("--splice_capture", help="If enabling ADC debug dump in firmware, use this feature to parse "
                                                 "the capture file into its individual csv captures.",
                        action="store_true")

    parser.add_argument("--send_csv", help="Send the CSV data we have on system to the firmware via UART.",
                        action="store_true")

    parser.add_argument("--test_csv", help="Send the CSV data we have generated to the firmware via UART.",
                        action="store_true")

    parser.add_argument("--decode_csv", help="Send the CSV data we have on system to the firmware via UART.",
                        action="store_true")

    parser.add_argument("--decode_db_tracing", help="Take a db UART trace and process out the tc data..",
                        action="store_true")

    args = parser.parse_args()

    if args.splice_capture:
        with open("data//capture_raw.txt", 'r', encoding='utf-8') as f:

            _lines = f.readlines()


        buffer_index = 0
        last_buffer_index = None
        _data = []
        _capture_number = 0

        for _line in _lines:
            #print(_line)

            # Decode the contents of a line
            try:
                _index_start = _line.index('[') + 1
                _index_end = _line.index(']')
                _adc = _line.split(":")[2][1:-1]
                print(_adc)

            except Exception as e:
                continue

            buffer_index = int(_line[_index_start:_index_end])
            #print("Buffer Index: {}, ADC: {}".format(buffer_index, _adc) )

            # Check if it is the first iterration through the loop or not
            if last_buffer_index is not None:

                if buffer_index != (last_buffer_index + 1) and buffer_index != 9999:
                    print("Lost data at {} {}".format(buffer_index, last_buffer_index))
                    last_buffer_index = None
                    _data = []

            last_buffer_index = buffer_index

            _data.append({"Index": buffer_index, "ADC":_adc})

            if buffer_index == 7999:
                last_buffer_index = None
                _df = pd.DataFrame(_data)
                _df.to_csv("data\\csv\\{}.csv".format(_capture_number))
                _capture_number += 1
                print(_data)
                _data = []


    if args.test_csv:

        time.sleep(3)
        print("Starting")

        _serial = serial.Serial("COM34", 2000000)
        #_serial.timeout = 0.0005
        _serial.timeout = 1
        _serial.set_buffer_size(rx_size=12800, tx_size=12800)

        _full_results = []

        # Get all the CSV file names ready to process.
        for d, subdir, files in os.walk("data\\csv_simulated"):

            files.reverse()

            for _file in files:
                print("Load {}".format(_file))
                _df = pd.read_csv(os.path.join("data\\csv_simulated", _file))

                _fileparts = _file[:-4].split("-")

                _results = {"low_level": _fileparts[0].split('_')[1],
                            "high_level": _fileparts[1].split('_')[1],
                            "slew": _fileparts[2].split('_')[1],
                            "noise": _fileparts[3].split('_')[1],
                            "pass": 0, "fail": 0, "failed_tuning": 0}

                for x in range(20):

                    print("Loop: {}".format(x))
                    _debug_string = ""

                    for index, row in _df.iterrows():
                        _msg = build_message("ADC={}".format(int(row['ADC'])))
                        _serial.write(_msg)
                        time.sleep(0.0000005)

                    time.sleep(1)
                    
                    # if index %100 == 0:
                    _data = _serial.readlines() # (_serial.inWaiting())
                    for _line in _data:
                        _debug_string += _line.decode("utf8")
                    print(_debug_string, sep="", end="")

                    if "Pass!" in _debug_string:
                        _results["pass"] += 1

                    elif "Fail!" in _debug_string:
                        _results["fail"] += 1

                    else:
                        #print(_data.decode("utf8"), sep="", end="")
                        _results["failed_tuning"] += 1

                    print(_results)
                _full_results.append(_results)
                _full_results_df = pd.DataFrame(_full_results)
                _full_results_df.to_csv("data\\full_results.csv")

    if args.send_csv:
        _serial = serial.Serial("COM32", 2000000)
        _serial.timeout = 0.05

        # Get all the CSV file names ready to process.
        for d, subdir, files in os.walk("data\\csv"):

            for _file in files:
                print("Load {}".format(_file))
                _df = pd.read_csv(os.path.join("data\\csv", _file))

                for index, row in _df.iterrows():

                    _msg = build_message("ADC={}".format(row['ADC']))
                    _serial.write(_msg)
                    # time.sleep(.1)

                    #while True:
                    _data = _serial.read(1000)
                    print(_data.decode("utf8"), sep="", end="")

                    #uart_encoder.send_packet(row['ADC'])
                    # _msg = build_message(row['ADC'])

    if args.decode_csv:
        # Get all the CSV file names ready to process.
        for d, subdir, files in os.walk("data\\csv"):

            for _file in files:
                _digital_data = []

                print("Process {}".format(_file))
                _df = pd.read_csv(os.path.join("data\\csv", _file))

                _atod = hardware.AnalogToDigital()

                for index, row in _df.iterrows():
                    _digital = _atod.check_pin_state(row['ADC'] >> 2)
                    _digital_data.append(_digital)

                _digital_data = _digital_data[15:]
                _digital_data.extend([0 for x in range(15)])

                #_series = pd.Series(_digital_data)
                _df['digital'] = _digital_data
                _df.to_csv(os.path.join("data\\digi", _file))

                fig = make_subplots(specs=[[{"secondary_y": True}]], rows=1, cols=1)

                fig = fig.update_layout(
                    title="Dave",
                    xaxis_title="Time",
                    yaxis_title="Digital Reading",
                    legend_title="Signals",
                    font=dict(
                        family="Courier New, monospace",
                        size=10,
                        color="RebeccaPurple"
                    ))

                fig = fig.add_trace(go.Scatter(x=_df.index, y=_df['digital'], name="digital"),
                                    row=1, col=1)
                fig = fig.add_trace(go.Scatter(x=_df.index, y=_df['ADC'], name="ADC"),
                                    row=1, col=1, secondary_y=True)
                fig.show()

                time.sleep(100)

    if args.decode_db_tracing:
        with open("data//audi.txt", 'r', encoding='utf-8') as f:

            _lines = f.readlines()

        buffer_index = 0
        last_buffer_index = None
        _data = []
        _capture_number = 0

        for _line in _lines:
            # print(_line)

            # Decode the contents of a line
            try:
                _index_start = _line.index('[') + 1
                _index_end = _line.index(']')
                _adc = _line.split(":")[2][1:-1]
                print(_adc)

            except Exception as e:
                continue

            buffer_index = int(_line[_index_start:_index_end])
            # print("Buffer Index: {}, ADC: {}".format(buffer_index, _adc) )

            # Check if it is the first iterration through the loop or not
            if last_buffer_index is not None:

                if buffer_index != (last_buffer_index + 1) and buffer_index != 9999:
                    print("Lost data at {} {}".format(buffer_index, last_buffer_index))
                    last_buffer_index = None
                    _data = []

            last_buffer_index = buffer_index

            _data.append({"Index": buffer_index, "ADC": _adc})

            if buffer_index == 7999:
                last_buffer_index = None
                _df = pd.DataFrame(_data)
                _df.to_csv("data\\csv\\{}.csv".format(_capture_number))
                _capture_number += 1
                print(_data)
                _data = []
