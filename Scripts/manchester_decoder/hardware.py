import time

class AnalogToDigital:
    # Constants
    PIN_UNDEFINED = -1
    PIN_LOW = 1
    PIN_MID_RANGE = 2
    PIN_HIGH = 3


    UPPER_QUINTILE = 650
    LOWER_QUINTILE = 150

    PAUSE_UPPER_QUINTILE = 700
    PAUSE_LOWER_QUINTILE = 100

    DEBOUNCE_COUNT = 15

    def __init__(self):
        self.pin_state = self.PIN_MID_RANGE
        self.last_pin_state = self.PIN_UNDEFINED

        self.time_between_pulses = 0
        self.debouncer = {self.PIN_MID_RANGE: 0, self.PIN_HIGH: 0, self.PIN_LOW: 0}

        # DEBUG CODE
        self.call_count = 0
        # DEBUG CODE END

    def reset_debouncer(self):
        self.debouncer = {self.PIN_MID_RANGE: 0, self.PIN_HIGH: 0, self.PIN_LOW: 0}

    def debounce(self, a_index):
        print("Debouncer: {}".format(self.debouncer))

        self.debouncer[a_index] += 1

        if self.debouncer[a_index] == self.DEBOUNCE_COUNT:
            self.debouncer[a_index] = 0
            return True

        else:
            return False

    def check_pin_state(self, a_adc_val):
        """
        // Desc: Check whether the envelope Rx pin has changed state in three possible states.
        // Inputs: ADC value we have measured
        // Return: None
        """
        # DEBUG CODE
        self.call_count += 1

        if self.call_count > 3400:
            #time.sleep(1)
            pass

        print("Check pin state for {}, call {}, state: {}".format(a_adc_val, self.call_count, self.pin_state))

        # DEBUG CODE END

        new_state = False

        # Check for a state change
        if self.pin_state != self.last_pin_state:
            new_state = True;
            self.last_pin_state = self.pin_state;

            self.time_between_pulses += 1;
            #debugf('.');

        if self.pin_state == self.PIN_MID_RANGE:

            if new_state:
                self.reset_debouncer();
                self.time_between_pulses = 0;

            if a_adc_val > self.PAUSE_UPPER_QUINTILE:
                print("PIN_MID_RANGE, ADC: {} > UPPER_QUIN: {}".format(a_adc_val, self.PAUSE_UPPER_QUINTILE))

                if self.debounce(self.PIN_HIGH):
                    print("PIN_MID_RANGE, Debounced. Move to PIN_HIGH")
                    # envelope_rx_process(self.pin_state, self.time_between_pulses);

                    self.pin_state = self.PIN_HIGH

            elif a_adc_val < self.PAUSE_LOWER_QUINTILE:
                print("PIN_MID_RANGE, ADC: {} < LOWER_QUIN: {}".format(a_adc_val, self.PAUSE_LOWER_QUINTILE))

                if self.debounce(self.PIN_LOW):
                    print("PIN_MID_RANGE, Debounced. Move to PIN_LOW")
                    # envelope_rx_process(self.pin_state, self.time_between_pulses);

                    self.pin_state = self.PIN_LOW

                else:
                    self.reset_debouncer()

        if self.pin_state == self.PIN_HIGH:

            if new_state:
                self.reset_debouncer()
                self.time_between_pulses = 0


            if ((a_adc_val < self.UPPER_QUINTILE) and (a_adc_val > self.LOWER_QUINTILE)):

                print("PIN_HIGH, ADC: {} in midrange".format(a_adc_val, self.PAUSE_UPPER_QUINTILE))

                if (self.debounce(self.PIN_MID_RANGE)):
                    print("PIN_HIGH, Debounced. Move to PIN_MID_RANGE")
                    # envelope_rx_process(self.pin_state, self.time_between_pulses);
                    self.pin_state = self.PIN_MID_RANGE

            elif (a_adc_val < self.LOWER_QUINTILE):
                print("PIN_HIGH, ADC: {} < LOWER_QUIN: {}".format(a_adc_val, self.LOWER_QUINTILE))

                if (self.debounce(self.PIN_LOW)):
                    print("PIN_HIGH, Debounced. Move to PIN_LOW")
                    # envelope_rx_process(self.pin_state, self.time_between_pulses);
                    self.pin_state = self.PIN_LOW;
            else:
                self.reset_debouncer();

        if self.pin_state == self.PIN_LOW:

            if new_state:
                self.reset_debouncer();
                self.time_between_pulses = 0;

            if ((a_adc_val < self.UPPER_QUINTILE) and (a_adc_val > self.LOWER_QUINTILE)):

                if (self.debounce(self.PIN_MID_RANGE)):
                    # envelope_rx_process(self.pin_state, self.time_between_pulses);
                    self.pin_state = self.PIN_MID_RANGE

            elif (a_adc_val > self.UPPER_QUINTILE):

                if (self.debounce(self.PIN_HIGH)):
                    # envelope_rx_process(self.pin_state, self.time_between_pulses);
                    self.pin_state = self.PIN_HIGH
            else:
                self.reset_debouncer()

        return self.pin_state