import pandas as pd
import time
import plotly.express as px
import numpy as np

def fletcher16(data):
    sum1 = 0xff
    sum2 = 0xff

    for x in range(len(data)):

        sum1 += data[x];
        sum2 += sum1;

        sum1 = (sum1 & 0xff) + (sum1 >> 8);
        sum2 = (sum2 & 0xff) + (sum2 >> 8);

    return [sum2, sum1];


class MancGenerator:
    #PIN_HIGH = 4095
    #PIN_MID = 2000
    #PIN_LOW = 0
    TEST_DATA = [0xA5, 0x5A, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0A, 0x0B, 0x0C]
    TEST_DATA = [0x00, 0x64, 0x00, 0xC8, 0x01, 0x90, 0x02, 0x58, 0x03, 0x20, 0x03, 0x84, 0x00, 0x00]

    def __init__(self, a_low_level, a_high_level, a_slew, a_noise):
        # Details of the pause state and pulse width
        self.pause_dur = 500
        self.pulse_width = 20

        # Details of the key variables
        self.slew = a_slew
        self.high_level = a_high_level
        self.low_level = a_low_level
        self.noise = a_noise

        self.preamble = [40, 40, 20, 20, 20, 20]

        self.adc_string = []

        if self.low_level > 0:
            self.dir = ((self.high_level - self.low_level) / 2) + self.low_level

        else:
            self.dir = self.high_level / 2

        self.last_dir = self.dir

    def generate(self):

        # Add the pause state variables
        self.adc_string = list(np.random.normal(self.dir, self.noise, self.pause_dur))
        self.dir = self.low_level

        # Add the preamble
        for x in range(len(self.preamble)):

            # Flip the data
            if self.dir == self.high_level:
                self.dir = self.low_level

            elif self.dir == self.low_level:
                self.dir = self.high_level

            self.adc_string.extend(list(np.random.normal(self.dir, self.noise, self.preamble[x])))

        self.encode_data(MancGenerator.TEST_DATA)

        # Calc the checksum
        _cs = fletcher16(MancGenerator.TEST_DATA)
        print(_cs)
        _cs.append(0x0A)

        self.encode_data(_cs)

    def encode_data(self, a_data):
        # After running
        for _byte in a_data:
            _bit_index = 0

            for _bit_index in range(7, -1, -1):

                _bit = _byte & (1 << _bit_index)
                # print("For byte: {}, bit_index: {}, _bit: {}".format(_byte, _bit_index, _bit))

                # We're got a bit high but are already high so clock low then high
                if self.dir == self.high_level and _bit:
                    # print("Clock low, data high")
                    self.add_pulse(self.low_level, self.pulse_width)
                    self.add_pulse(self.high_level, self.pulse_width)

                # We've got a pin low and are already low so clock the bits
                elif self.dir == self.low_level and not _bit:
                    # print("Clock high, data low")
                    self.add_pulse(self.high_level, self.pulse_width)
                    self.add_pulse(self.low_level, self.pulse_width)

                else:
                    # print("No clock, data")

                    # Flip the data
                    if self.dir == self.high_level:
                        self.dir = self.low_level

                    elif self.dir == self.low_level:
                        self.dir = self.high_level

                    self.add_pulse(self.dir, self.pulse_width * 2)

    def add_pulse(self, a_dir, a_dur):

        if self.slew > 0:
            _dur = a_dur - self.slew
            _slew_delta = ((self.high_level - self.low_level) / 2) / self.slew

            if a_dir == self.high_level:
                _up = True

            else:
                _up = False

        else:
            _dur = a_dur

        # Add the pulse width
        self.adc_string.extend(list(np.random.normal(a_dir, self.noise, _dur)))

        #for x in range(_dur):
        #    self.adc_string.append(a_dir)
        _value = self.adc_string[-1]

        # Add the pulse width
        for x in range(self.slew):
            if _up:
                _value -= _slew_delta

            else:
                _value += _slew_delta

            self.adc_string.append(_value)

        # Flip the data
        self.dir = a_dir

    def check_for_clipping(self):

        for x in range(len(self.adc_string)):

            if self.adc_string[x] < 0:
                self.adc_string[x] = 0

            if self.adc_string[x] > 4095:
                self.adc_string[x] = 4095

if __name__ == "__main__":
    AMP_MAX = 4000
    AMP_MIN = 2000
    AMP_STEP = -500

    SLEW_MAX = 6
    SLEW_MIN = 0
    SLEW_STEP = 1

    NOISE_MAX = 250
    NOISE_MIN = 0
    NOISE_STEP = 50

    # Generate different amplitudes
    for x in range(AMP_MAX, AMP_MIN-1, AMP_STEP):
        _manc_data = MancGenerator(a_low_level=0, a_high_level=x, a_slew=0, a_noise=0)

        _manc_data.generate()
        _manc_data.check_for_clipping()

        _df = pd.DataFrame({"ADC": _manc_data.adc_string})
        _df.to_csv("data\\csv_simulated\\ll_{}-hl_{}-slew_{}-noise_{}.csv".format(0, x, 0, 0))

        if x == AMP_MIN:
            fig = px.line(_df, x=_df.index, y="ADC", range_y=[0, 4200])
            fig.show()

    # Generate different slews
    for x in range(SLEW_MIN, SLEW_MAX+1, SLEW_STEP):
        _manc_data = MancGenerator(a_low_level=0, a_high_level=AMP_MAX, a_slew=x, a_noise=0)

        _manc_data.generate()
        _manc_data.check_for_clipping()

        _df = pd.DataFrame({"ADC": _manc_data.adc_string})
        _df.to_csv("data\\csv_simulated\\ll_{}-hl_{}-slew_{}-noise_{}.csv".format(0, AMP_MAX, x, 0))
        #fig = px.line(_df, x=_df.index, y="ADC")
        #fig.show()

        if x == SLEW_MAX:
            fig = px.line(_df, x=_df.index, y="ADC", range_y=[0, 4200])
            fig.show()

    # Generate different levels of noise
    for x in range(NOISE_MIN, NOISE_MAX+1, NOISE_STEP):
        _manc_data = MancGenerator(a_low_level=0, a_high_level=AMP_MAX, a_slew=0, a_noise=x)

        _manc_data.generate()
        _manc_data.check_for_clipping()

        _df = pd.DataFrame({"ADC": _manc_data.adc_string})
        _df.to_csv("data\\csv_simulated\\ll_{}-hl_{}-slew_{}-noise_{}.csv".format(0, AMP_MAX, 0, x))

        if x == NOISE_MAX:
            fig = px.line(_df, x=_df.index, y="ADC", range_y=[0, 4200])
            fig.show()


    # Amp and Slew
    for x in range(AMP_MAX, AMP_MIN-1, AMP_STEP):
        for y in range(SLEW_MIN, SLEW_MAX+1, SLEW_STEP):
            _manc_data = MancGenerator(a_low_level=0, a_high_level=x, a_slew=y, a_noise=0)

            _manc_data.generate()
            _manc_data.check_for_clipping()

            _df = pd.DataFrame({"ADC": _manc_data.adc_string})
            _df.to_csv("data\\csv_simulated\\ll_{}-hl_{}-slew_{}-noise_{}.csv".format(0, x, y, 0))

            if x == AMP_MIN and y == SLEW_MAX:
                fig = px.line(_df, x=_df.index, y="ADC", range_y=[0, 4200])
                fig.show()

    # Amp and noise
    for x in range(AMP_MAX, AMP_MIN-1, AMP_STEP):
        for y in range(NOISE_MIN, NOISE_MAX+1, NOISE_STEP):
            _manc_data = MancGenerator(a_low_level=0, a_high_level=x, a_slew=0, a_noise=y)

            _manc_data.generate()
            _manc_data.check_for_clipping()

            _df = pd.DataFrame({"ADC": _manc_data.adc_string})
            _df.to_csv("data\\csv_simulated\\ll_{}-hl_{}-slew_{}-noise_{}.csv".format(0, x, 0, y))

            if x == AMP_MIN and y == NOISE_MAX:
                fig = px.line(_df, x=_df.index, y="ADC", range_y=[0, 4200])
                fig.show()

    # Slew and noise
    for x in range(SLEW_MIN, SLEW_MAX+1, SLEW_STEP):
        for y in range(NOISE_MIN, NOISE_MAX+1, NOISE_STEP):
            _manc_data = MancGenerator(a_low_level=0, a_high_level=AMP_MAX, a_slew=x, a_noise=y)

            _manc_data.generate()
            _manc_data.check_for_clipping()

            _df = pd.DataFrame({"ADC": _manc_data.adc_string})
            _df.to_csv("data\\csv_simulated\\ll_{}-hl_{}-slew_{}-noise_{}.csv".format(0, AMP_MAX, x, y))

            if x == SLEW_MAX and y == NOISE_MAX:
                fig = px.line(_df, x=_df.index, y="ADC", range_y=[0, 4200])
                fig.show()


    # Amp and Slew and Noise
    for x in range(AMP_MAX, AMP_MIN-1, AMP_STEP):
        for y in range(SLEW_MIN, SLEW_MAX+1, SLEW_STEP):
            for z in range(NOISE_MIN, NOISE_MAX+1, NOISE_STEP):
                _manc_data = MancGenerator(a_low_level=0, a_high_level=x, a_slew=y, a_noise=z)

                _manc_data.generate()
                _manc_data.check_for_clipping()

                _df = pd.DataFrame({"ADC": _manc_data.adc_string})
                _df.to_csv("data\\csv_simulated\\ll_{}-hl_{}-slew_{}-noise_{}.csv".format(0, x, y, z))

                if x == AMP_MIN and y == SLEW_MAX and z == NOISE_MAX:
                    fig = px.line(_df, x=_df.index, y="ADC", range_y=[0, 4200])
                    fig.show()
