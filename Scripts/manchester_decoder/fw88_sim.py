import serial
import time

if __name__ == "__main__":

    _serial = serial.Serial("COM34", 2000000)
    _serial.timeout = 1

    while True:

        _test_data = bytearray([0xA5, 0x5A, 0x19, 0x10, 0x01, 0x10, 0x00, 0x00, 0x00, 0x00, 0x01, 0xA5, 0x5A, 0x01,
                                0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x13, 0x4E])

        serial.write(_test_data)
        time.sleep(1)