//****************************************************************************
//
// Guard
//
//****************************************************************************

#ifndef HARDWARE_H
#define HARDWARE_H

////******************************************************************************
//
// Basic Board Details
//
////******************************************************************************


#define PCB_NUMBER_70 70 // pcb identifier for configs
#define PCB_NUMBER_88 88 // pcb identifier for configs

#define PCB_NUMBER PCB_NUMBER_88

#define PCB_STRING "PCB0088"


//******************************************************************************
//
// System Clock Frequency
//
//******************************************************************************

// System Clock
#define PROCESSOR_CLOCK         80000000

// System Tick
#define SYSTICKHZ                       1000
#define SYSTICK_PERIOD_US               (1000000/SYSTICKHZ) 


//******************************************************************************
//
// GPIO - Output Pins
//
//******************************************************************************

#if PCB_NUMBER == PCB_NUMBER_88

    #define LED_PERIPH                  SYSCTL_PERIPH_GPIOL
    #define LED_BASE        GPIO_PORTL_BASE    

    #define LED_1           GPIO_PIN_0
    #define LED_2           GPIO_PIN_1
    #define LED_3           GPIO_PIN_2
    #define LED_4           GPIO_PIN_3
    #define LED_5           GPIO_PIN_4
    #define LED_6           GPIO_PIN_5

#elif PCB_NUMBER == PCB_NUMBER_70
    #define LED_PERIPH                  SYSCTL_PERIPH_GPIOE
    #define LED_BASE        GPIO_PORTE_BASE    

    #define LED_1           GPIO_PIN_4
    #define LED_2           GPIO_PIN_5
    
    #define LED_3           GPIO_PIN_6
    #define LED_4           GPIO_PIN_6
    #define LED_5           GPIO_PIN_6
    #define LED_6           GPIO_PIN_6
    
    
#else
    #error "UNKNOWN PCB_REV"
#endif


//******************************************************************************
//
// UARTS
//
//******************************************************************************

typedef enum 
{
    DEBUG_PORT_UART = 0,
    ARM_TO_ARM_UART,
    ARM_TO_PIC1_UART,
    ARM_TO_PIC2_UART,
    ARM_TO_PIC3_UART,
    ARM_TO_PIC4_UART,
    ARM_TO_HYBRID1_UART,
    ARM_TO_HYBRID2_UART,
    NUMBER_OF_UARTS     // This should always be last in the ENUM list
} UART_INDEXES;

#if PCB_NUMBER == PCB_NUMBER_88

// Debug is UART0
#define DEBUG_UART_PERIPH                       SYSCTL_PERIPH_UART0
#define DEBUG_GPIO_PERIPH                       SYSCTL_PERIPH_GPIOA
#define DEBUG_GPIO_BASE                         GPIO_PORTA_BASE
#define DEBUG_GPIO_TXPIN                        GPIO_PIN_1
#define DEBUG_GPIO_RXPIN                        GPIO_PIN_0
#define DEBUG_UART_INT                          INT_UART0
#define DEBUG_UART_CONFIG                       (UART_CONFIG_WLEN_8 | UART_CONFIG_STOP_ONE | UART_CONFIG_PAR_NONE)
#define DEBUG_UART_BASE                         UART0_BASE
#define DEBUG_BAUD                              2000000
#define DEBUG_UART_TXCONFPIN                    GPIO_PA1_U0TX
#define DEBUG_UART_RXCONFPIN                    GPIO_PA0_U0RX
#define DEBUG_UART_INT_ENABLED                  1
#define DEBUG_UART_INT_PRIO                     8

#elif PCB_NUMBER == PCB_NUMBER_70

#define DEBUG_UART_PERIPH                       SYSCTL_PERIPH_UART1
#define DEBUG_GPIO_PERIPH                       SYSCTL_PERIPH_GPIOC
#define DEBUG_GPIO_BASE                         GPIO_PORTC_BASE
#define DEBUG_GPIO_TXPIN                        GPIO_PIN_5
#define DEBUG_GPIO_RXPIN                        GPIO_PIN_4
#define DEBUG_UART_INT                          INT_UART1
#define DEBUG_UART_CONFIG                       (UART_CONFIG_WLEN_8 | UART_CONFIG_STOP_ONE | UART_CONFIG_PAR_NONE)
#define DEBUG_UART_BASE                         UART1_BASE
#define DEBUG_BAUD                              2000000
#define DEBUG_UART_TXCONFPIN                    GPIO_PC5_U1TX
#define DEBUG_UART_RXCONFPIN                    GPIO_PC4_U1RX
#define DEBUG_UART_INT_ENABLED                  1
#define DEBUG_UART_INT_PRIO                     8

#endif

// ARM_TO_ARM is UART2
#define ARM_TO_ARM_UART_PERIPH                  SYSCTL_PERIPH_UART2
#define ARM_TO_ARM_GPIO_PERIPH                  SYSCTL_PERIPH_GPIOG
#define ARM_TO_ARM_GPIO_BASE                    GPIO_PORTG_BASE
#define ARM_TO_ARM_GPIO_TXPIN                   GPIO_PIN_5
#define ARM_TO_ARM_GPIO_RXPIN                   GPIO_PIN_4
#define ARM_TO_ARM_UART_INT                     INT_UART2
#define ARM_TO_ARM_UART_CONFIG                  (UART_CONFIG_WLEN_8 | UART_CONFIG_STOP_ONE | UART_CONFIG_PAR_NONE)
#define ARM_TO_ARM_UART_BASE                    UART2_BASE
#define ARM_TO_ARM_BAUD                         115200
#define ARM_TO_ARM_UART_TXCONFPIN               GPIO_PG5_U2TX
#define ARM_TO_ARM_UART_RXCONFPIN               GPIO_PG4_U2RX
#define ARM_TO_ARM_UART_INT_ENABLED             1
#define ARM_TO_ARM_UART_INT_PRIO                16

// ARM_TO_PIC1 is UART3
#define ARM_TO_PIC1_UART_PERIPH                 SYSCTL_PERIPH_UART3
#define ARM_TO_PIC1_GPIO_PERIPH                 SYSCTL_PERIPH_GPIOC
#define ARM_TO_PIC1_GPIO_BASE                   GPIO_PORTC_BASE
#define ARM_TO_PIC1_GPIO_TXPIN                  GPIO_PIN_7
#define ARM_TO_PIC1_GPIO_RXPIN                  GPIO_PIN_6
#define ARM_TO_PIC1_UART_INT                    INT_UART3
#define ARM_TO_PIC1_UART_CONFIG                 (UART_CONFIG_WLEN_8 | UART_CONFIG_STOP_ONE | UART_CONFIG_PAR_NONE)
#define ARM_TO_PIC1_UART_BASE                   UART3_BASE
#define ARM_TO_PIC1_BAUD                        76800
#define ARM_TO_PIC1_UART_TXCONFPIN              GPIO_PC7_U3TX
#define ARM_TO_PIC1_UART_RXCONFPIN              GPIO_PC6_U3RX
#define ARM_TO_PIC1_UART_INT_ENABLED            0
#define ARM_TO_PIC1_UART_INT_PRIO               8

// ARM_TO_PIC2 is UART4
#define ARM_TO_PIC2_UART_PERIPH                 SYSCTL_PERIPH_UART4
#define ARM_TO_PIC2_GPIO_PERIPH                 SYSCTL_PERIPH_GPIOJ
#define ARM_TO_PIC2_GPIO_BASE                   GPIO_PORTJ_BASE
#define ARM_TO_PIC2_GPIO_TXPIN                  GPIO_PIN_1
#define ARM_TO_PIC2_GPIO_RXPIN                  GPIO_PIN_0
#define ARM_TO_PIC2_UART_INT                    INT_UART4
#define ARM_TO_PIC2_UART_CONFIG                 (UART_CONFIG_WLEN_8 | UART_CONFIG_STOP_ONE | UART_CONFIG_PAR_NONE)
#define ARM_TO_PIC2_UART_BASE                   UART4_BASE
#define ARM_TO_PIC2_BAUD                        76800
#define ARM_TO_PIC2_UART_TXCONFPIN              GPIO_PJ1_U4TX
#define ARM_TO_PIC2_UART_RXCONFPIN              GPIO_PJ0_U4RX
#define ARM_TO_PIC2_UART_INT_ENABLED            0
#define ARM_TO_PIC2_UART_INT_PRIO               8

// ARM_TO_PIC3 is UART5
#define ARM_TO_PIC3_UART_PERIPH                 SYSCTL_PERIPH_UART5
#define ARM_TO_PIC3_GPIO_PERIPH                 SYSCTL_PERIPH_GPIOJ
#define ARM_TO_PIC3_GPIO_BASE                   GPIO_PORTJ_BASE
#define ARM_TO_PIC3_GPIO_TXPIN                  GPIO_PIN_3
#define ARM_TO_PIC3_GPIO_RXPIN                  GPIO_PIN_2
#define ARM_TO_PIC3_UART_INT                    INT_UART5
#define ARM_TO_PIC3_UART_CONFIG                 (UART_CONFIG_WLEN_8 | UART_CONFIG_STOP_ONE | UART_CONFIG_PAR_NONE)
#define ARM_TO_PIC3_UART_BASE                   UART5_BASE
#define ARM_TO_PIC3_BAUD                        76800
#define ARM_TO_PIC3_UART_TXCONFPIN              GPIO_PJ3_U5TX
#define ARM_TO_PIC3_UART_RXCONFPIN              GPIO_PJ2_U5RX
#define ARM_TO_PIC3_UART_INT_ENABLED            0
#define ARM_TO_PIC3_UART_INT_PRIO               8

// ARM_TO_PIC4 is UART6
#define ARM_TO_PIC4_UART_PERIPH                 SYSCTL_PERIPH_UART6
#define ARM_TO_PIC4_GPIO_PERIPH                 SYSCTL_PERIPH_GPIOJ
#define ARM_TO_PIC4_GPIO_BASE                   GPIO_PORTJ_BASE
#define ARM_TO_PIC4_GPIO_TXPIN                  GPIO_PIN_5
#define ARM_TO_PIC4_GPIO_RXPIN                  GPIO_PIN_4
#define ARM_TO_PIC4_UART_INT                    INT_UART6
#define ARM_TO_PIC4_UART_CONFIG                 (UART_CONFIG_WLEN_8 | UART_CONFIG_STOP_ONE | UART_CONFIG_PAR_NONE)
#define ARM_TO_PIC4_UART_BASE                   UART6_BASE
#define ARM_TO_PIC4_BAUD                        76800
#define ARM_TO_PIC4_UART_TXCONFPIN              GPIO_PJ5_U6TX
#define ARM_TO_PIC4_UART_RXCONFPIN              GPIO_PJ4_U6RX
#define ARM_TO_PIC4_UART_INT_ENABLED            0
#define ARM_TO_PIC4_UART_INT_PRIO               8

// ARM_TO_HYBRID1_UART is UART7
#define ARM_TO_HYBRID1_UART_PERIPH              SYSCTL_PERIPH_UART7
#define ARM_TO_HYBRID1_GPIO_PERIPH              SYSCTL_PERIPH_GPIOE
#define ARM_TO_HYBRID1_UART_GPIO_BASE           GPIO_PORTE_BASE
#define ARM_TO_HYBRID1_UART_GPIO_TXPIN          GPIO_PIN_1
#define ARM_TO_HYBRID1_UART_GPIO_RXPIN          GPIO_PIN_0
#define ARM_TO_HYBRID1_UART_INT                 INT_UART7
#define ARM_TO_HYBRID1_UART_CONFIG              (UART_CONFIG_WLEN_8 | UART_CONFIG_STOP_ONE | UART_CONFIG_PAR_NONE)
#define ARM_TO_HYBRID1_UART_BASE                UART7_BASE
#define ARM_TO_HYBRID1_UART_BAUD                76800
#define ARM_TO_HYBRID1_UART_TXCONFPIN           GPIO_PE1_U7TX
#define ARM_TO_HYBRID1_UART_RXCONFPIN           GPIO_PE0_U7RX
#define ARM_TO_HYBRID1_UART_INT_ENABLED         0
#define ARM_TO_HYBRID1_UART_INT_PRIO            8

// ARM_TO_HYBRID2_UART is UART1
#if 0
#define ARM_TO_HYBRID2_UART_PERIPH              SYSCTL_PERIPH_UART1
#define ARM_TO_HYBRID2_GPIO_PERIPH              SYSCTL_PERIPH_GPIOC
#define ARM_TO_HYBRID2_UART_GPIO_BASE           GPIO_PORTC_BASE
#define ARM_TO_HYBRID2_UART_GPIO_TXPIN          GPIO_PIN_5
#define ARM_TO_HYBRID2_UART_GPIO_RXPIN          GPIO_PIN_4
#define ARM_TO_HYBRID2_UART_INT                 INT_UART1
#define ARM_TO_HYBRID2_UART_CONFIG              (UART_CONFIG_WLEN_8 | UART_CONFIG_STOP_ONE | UART_CONFIG_PAR_NONE)
#define ARM_TO_HYBRID2_UART_BASE                UART1_BASE
#define ARM_TO_HYBRID2_UART_BAUD                76800
#define ARM_TO_HYBRID2_UART_TXCONFPIN           GPIO_PC5_U1TX
#define ARM_TO_HYBRID2_UART_RXCONFPIN           GPIO_PC4_U1RX
#define ARM_TO_HYBRID2_UART_INT_ENABLED         0
#define ARM_TO_HYBRID2_UART_INT_PRIO            8
#endif


//****************************************************************************
//
// Defines
//
//****************************************************************************

#define CONSOLE_OUTPUT
#define DEBUG

#define NUMINPUTS                   4

#define CMD_CHECK	                0
#define RF_INPUT1	                1
#define RF_INPUT2                   2
#define RF_INPUT3                   3
#define RF_INPUT4                   4
#define RF_INPUT5                   5
#define RF_OUTPUT                   6
                
#define NUMUARTS	                8

#define UNIX_TIME_CHECK             1293753600u // represents a "recent" time to ensure clock is at least vaguely set

// buffers used for cacheing ADC result and storing remaining data
#define READBUFFER_SIZE             200
#define WRITEBUFFER_SIZE            25000

// Timer clock cycles used to set ADC sample rates
#define ADC_LOW_SAMPLERATE          640
#define ADC_MID_SAMPLERATE          320
#define ADC_HIGH_SAMPLERATE         160

//Interupt priorities upper 3 bits 0x00 = Highest, 0xE0 = Lowest
#define INT_PRIORITY_UART       	0xE0
#define INT_PRIORITY_ADCTIMER   	0x00
#define INT_PRIORITY_UART_CMD       0x50
#define INT_PRIORITY_DIGIINPUTS     0x20

// maximum length of UART data  
#define UART_MAX_DATA_LEN           100


#endif
