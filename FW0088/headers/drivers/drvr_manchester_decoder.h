

//****************************************************************************
//
// Guard
//
//****************************************************************************

#ifndef ENVOLOPERX_H
#define	ENVOLOPERX_H

#ifdef	__cplusplus
extern "C" {
#endif


//****************************************************************************
//
// Defines
//
//****************************************************************************

#define HIGH                    true
#define LOW                     false
        
#define DATA_LEN                15
#define CS_LEN                  2
    
#define SHORT_PULSE             1
#define LONG_PULSE              2

// Horizontal axis calibration to determine data and clocks
#define INDEX_PULSE                                 20
    
#define LONG_LONG_PULSE                             40
#define LONG_LONG_TOLERANCE_UPPER                   9
#define LONG_LONG_TOLERANCE_LOWER                   9
    
#define SHORT_LONG_PULSE                            40
#define SHORT_LONG_TOLERANCE_UPPER                  9
#define SHORT_LONG_TOLERANCE_LOWER                  9
    
#define LONG_SHORT_PULSE                            20
#define LONG_SHORT_TOLERANCE                        9
    
#define SHORT_SHORT_PULSE                           20
#define SHORT_SHORT_TOLERANCE                       9

// Indexes of pulse types.       
#define SHORT_SHORT_INDEX                           0
#define SHORT_LONG_INDEX                            1
#define LONG_SHORT_INDEX                            2
#define LONG_LONG_INDEX                             3


//****************************************************************************
//
// Typedefs
//
//****************************************************************************

// Typedef for the state machine
typedef enum EnvelopeDecoder
{
    STATE_IDLE = 1,
    STATE_PAUSE,
    STATE_PREAMBLE,
    STATE_DATA,
    //STATE_FAULT
    STATE_PAUSE_LOST,
    STATE_PREAMBLE_LOST,
    STATE_DATA_LOST,
    STATE_CS_FAIL
} EnvelopeDecoder;

typedef struct Pulse
{
    bool dir;
    uint16_t dur; 
} Pulse;

typedef struct PulseTypes
{
    char type[3]; 
    uint16_t duration;
    uint8_t count;
    uint8_t upper_tol;
    uint8_t lower_tol;
} PulseTypes;


typedef struct PulseProfile
{
    bool dir;
    uint16_t dur; 
    uint8_t upper_tol;
    uint8_t lower_tol;
} PulseProfile;

typedef enum FaultCodes
{
    PAUSE_LOST = 100,
    PREAMBLE_LOST,
    DATA_LOST,
    CHECKSUM_FAILED
} FaultCodes;

typedef struct PulseType
{
    uint16_t static_dur; 
    uint16_t tuned_dur; 
    uint8_t upper_tol;
    uint8_t lower_tol;
} PulseType;

// This structure is used to aggregate successful pulse decodes of each pulse type
typedef struct PulseStats
{
    uint32_t total;
    uint32_t count;
    uint16_t min;
    uint16_t max;
} PulseStats;
    
typedef struct EnvelopePulseDebug
{
    Pin_states_t dir;
    uint16_t dur; 
    EnvelopeDecoder state;
} EnvelopePulseDebug;


//****************************************************************************
//
// Variable Declarations
//
//****************************************************************************

extern bool g_envelope_data_available;
extern uint8_t g_rx_buff[100];
extern uint8_t g_rx_buff_holding[30];
extern EnvelopeDecoder g_env_state;


//****************************************************************************
//
// Function Declarations
//
//****************************************************************************

void envelope_rx_process(Pin_states_t a_dir, uint16_t a_dur);


#ifdef	__cplusplus
}
#endif

#endif	/* ENVOLOPERX_H */

