

//****************************************************************************
//
// Guard
//
//****************************************************************************


//****************************************************************************
//
// Defines
//
//****************************************************************************

#define DEBOUNCE_LOW                0
#define DEBOUNCE_HIGH               1
#define DEBOUNCE_MID_RANGE          2

// Tuning Variables
#define DEBOUNCE                10
#define PULSE_DURATION          300
#define SUCCESSIVE_FAILURES     10
    
// Amplification Calc Tolerance
#define ADC_STEP_NOISE                      (50 << 2)
#define ADC_THRESHOLD_HYSTERESIS            (100 << 2)
#define ADC_MIN_AMPLIFICATION_DIFF          (400 << 2)
#define ADC_STANDARD_DEVIATION_THRESHOLD    (15 << 2)

#define DEBUG_PULSES_BUFF_LEN                       140   

// Buffer len define
#define STATS_BUFF          15
#define STEADY_STATE_BUFF   50

//****************************************************************************
//
// Typedefs
//
//****************************************************************************

typedef enum Pin_states{
    PIN_LOW,
    PIN_HIGH,
    PIN_MID_RANGE        
} Pin_states_t;

// This structure is used to aggregate successful pulse decodes of each pulse type
typedef struct ADCStats
{
    uint32_t total;
    uint32_t count;
    uint16_t min;
    uint16_t max;
    uint16_t mean;
    uint16_t vari;
    uint16_t stand_dev;
} ADCStats;

typedef struct SteadyState
{
    uint32_t mean;
    uint32_t count;
} SteadyState;

//****************************************************************************
//
// Variable Declarations
//
//****************************************************************************


//****************************************************************************
//
// Function Declarations
//
//****************************************************************************

void check_pin_state(uint16_t a_adc_val);
void reset_amplitude_thresholds(void);
