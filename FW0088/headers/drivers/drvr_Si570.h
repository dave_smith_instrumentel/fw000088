/*
    A Library to handle the programming of a reprogamable crystal/clock
    Part si570
*/

//****************************************************************************
//
// Guard
//
//****************************************************************************

#ifndef SI570_H
#define SI570_H

//****************************************************************************
//
// Defines
//
//****************************************************************************

#define CHAN_START CN_CH1
#define CHAN_END CN_CH5

#define NUM_CHANNELS	        4
#define SLAVE_ADDR		        0x55

//Define Registers for Si570
#define REG_7					0x07
#define REG_8					0x08
#define REG_9					0x09
#define REG_10				    0x0A
#define REG_11				    0x0B
#define REG_12				    0x0C
#define REG_135				    0x87
#define REG_137				    0x89

#define POW_2_28			    268435456.00
#define FCORANGE_LOW	        4850.0
#define FCORANGE_HIGH	        5670.0

//#define FARFIELD_CARD_1
//#define FARFIELD_CARD_2

#define C1_OE_PIN               GPIO_PIN_4
#define C1_OE_PORTBASE          GPIO_PORTN_BASE
#define C1_OE_PERIPH            SYSCTL_PERIPH_GPION

#define PIN_LOW                 0x00
#define PIN_HIGH                0xFF
        
#define I2C_PERIPH              SYSCTL_PERIPH_I2C0
#define I2C_GPIO_PERIPH         SYSCTL_PERIPH_GPIOB
#define I2C_BASE                I2C0_BASE
#define I2C_PINCON_CLK          GPIO_PB2_I2C0SCL
#define I2C_PINCON_DAT          GPIO_PB3_I2C0SDA
#define I2C_GPIO_BASE           GPIO_PORTB_BASE
#define I2C_PIN_DAT             GPIO_PIN_3
#define I2C_PIN_CLK             GPIO_PIN_2

#define FXTAL_NOM				114.285	//MHz
#define SI570_REGLEN            6

//****************************************************************************
//
// Typedefs
//
//****************************************************************************

typedef enum { 
    CN_CH1 = 0, 
    CN_CH2, 
    CN_CH3,
    CN_CH4, 
    CN_CH5
} ChanNum; 

struct Si570_Params
{
	unsigned char reg[SI570_REGLEN];
	unsigned char N1;
	unsigned char HS_DIV;
	unsigned long long ulRFREQ;
	double RFREQ;
	double	FXTAL;
	double FOUT;
	unsigned char def_N1;
	unsigned char def_HS_DIV;
	unsigned long long def_ulRFREQ;
	double def_RFREQ;
	double def_FOUT;
};

extern struct Si570_Params Si570Channels[NUM_CHANNELS];

//****************************************************************************
//
// Function Declarations
//
//****************************************************************************

void init_Si570( void );

uint32_t I2CReceive(uint32_t channel, uint8_t reg);
void I2CSend(uint8_t channel, uint8_t reg, uint8_t data);
void init_i2c0(void);

void ConfigOutputFreqChannels( struct Si570_Params *Si570Channels, ChanNum channel );
void ReadStartupConfigChannels ( struct Si570_Params *Si570Channels, ChanNum channel );
uint32_t SelectChannel(unsigned char channel);
void ReadConfigRegsChannels ( struct Si570_Params *Si570Channels, ChanNum channel );

#endif
