//****************************************************************************
//
// Guard
//
//****************************************************************************

#ifndef __UART_HW_H__
#define __UART_HW_H__


//****************************************************************************
//
// Includes
//
//****************************************************************************

#include <stdint.h>
#include <stdbool.h>


//****************************************************************************
//
// Defines
//
//****************************************************************************

#define UART_PREAMBLE_INDEX_NULL 0xFFFF // 16 bit
#define PREAMBLE_INDEX_SIZE 25

#define UART_PARITY_NONE    0
#define UART_PARITY_ODD     1
#define UART_PARITY_EVEN    2
#define UART_PARITY_ONE     4
#define UART_PARITY_ZERO    5
    
// Reg-ID
#define UART_REGID_PREAMBLEBUFFER           1   
#define UART_REGID_MAINBUFFER               2
#define UART_REGID_BYTES_OVERFLOW           3
#define UART_REGID_BYTES_RXTOTAL            4
#define UART_REGID_BYTES_SINCEPREAMBLE      5
#define UART_REGID_RXERR_TIMEOUTS           6
#define UART_REGID_RXERR_PARITY             7
#define UART_REGID_RXERR_FRAMING            8
#define UART_REGID_RXERR_OVERRUN            9   
#define UART_REGID_BYTES_PREAMBLECOUNTER   10  
#define UART_REGID_RXERR_PREAMBLEOVERFLOW  11 
#define UART_REGID_FLAG_TXWASCLEAR         12
#define UART_REGID_BYTES_TXINSEQ           13

#define UART_ACTID_TXRESOURCE               1

#define UART_TXFEATURE_NONE                 0x0000
#define UART_TXFEATURE_DRIVELINE            0x0001
#define UART_TXFEATURE_CLEARLINE            0x0002
#define UART_TXFEATURE_CLEARLINE_BUSYHIGH   0x0004
#define UART_TXFEATURE_DRIVELINE_ENABLEHIGH 0x0008
#define UART_TXFEATURE_CLEARLINE_FLUSHBUSY  0x0010

#define UART_BITFLAG_TXWASCLEAR             0x0001
#define UART_BITFLAG_RXBUFFERFLUSH          0x0002


//****************************************************************************
//
// Typedefs
//
//****************************************************************************

typedef struct UART_init_struct
{
    uint32_t uart_peripheral;
    uint32_t gpio_peripheral;
    uint32_t uart_gpio_base;
    uint32_t tx_pin;
    uint32_t rx_pin;
    uint32_t uart_int;
    uint32_t uart_config;
    uint32_t uart_base;
    uint32_t baud;
    uint32_t tx_conf_pin;
    uint32_t rx_conf_pin;
    uint32_t interupt_enabled;
    uint32_t interupt_priority;
} uart_init_struct;

typedef struct UARTEncoderTag
{
    uint32_t uart_base;
} UARTEncoderTag;

typedef struct UartChannel
{
    unsigned long channel;
    unsigned long uart_base;
    unsigned long uart_peripheral;
    unsigned long gpio_base;
    unsigned long gpio_peripheral;
    unsigned long tx_pin;
    unsigned long rx_pin;
    unsigned long tx_confpin;
    unsigned long rx_confpin;
    unsigned long uart_int;
}UartChannel;

// this config comes after the UartChannelConfig (and preamble, if there is one)
// this is only present if the align_a is flaged to have this. 
typedef struct UartChannelConfigTransmitter
{
    uint32_t delay_preTX;         // This is a time held in us...
    uint32_t delay_postTX;        // This is a time held in us... 
    uint32_t delay_interbyteTX;
    uint16_t features;
    uint16_t drive_pin;         // A digital pin to drive the 
    uint16_t clear_pin;         // A digital pin to read if the clear flag is there
    uint16_t align_a;
    
}UartChannelConfigTransmitter;  

typedef struct UARTCopyBufferArgs
{
    volatile uint16_t * from_index_write;
    volatile uint16_t * from_index_read;
    uint8_t           * from_buff;
    uint16_t            from_buff_size;
    uint8_t           * to_buff;
    uint16_t            to_buff_size;
}UARTCopyBufferArgs;

//****************************************************************************
//
// Declarations
//
//****************************************************************************

void uarthw_init(uint32_t a_index);
void uarthw_send(uint32_t a_index, uint8_t *data, uint32_t len);
void uarthw_send_string(const uint8_t * buff,  uint32_t len, void * ud);

void uart_init( void );
int32_t uart_setupPeripherals( void );

#endif
