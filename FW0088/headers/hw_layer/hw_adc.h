#ifndef ADC_H
#define ADC_H

#define NO_ADC_BITS     12
#define ADC_MAX_COUNTS  ( 2 ^ NO_ADC_BITS )
#define ADC_MIDPOINT    ( ADC_MAX_COUNTS/2 )

#define CH1             1
#define CH2             2
#define CH3             3
#define CH4             4

// These defines make it easier to read the code
// rather than using multiple magic numbers in
// one function call
#define SEQ0            0
#define SEQ1            1
#define SEQ2            2
#define SEQ3            3

#define STEP0           0
#define STEP1           1
#define STEP2           2
#define STEP3           3
#define STEP4           4
#define STEP5           5
#define STEP6           6
#define STEP7           7

#define PRIORITY0       0
#define PRIORITY1       1
#define PRIORITY2       2
#define PRIORITY3       3

#define SIZEOF_ADC_DATA 4     // samples in use per sequencer

#define SAMPLE_BUFF_LEN 8000

extern volatile uint16_t g_sample_buff[SAMPLE_BUFF_LEN + 200];
extern uint32_t g_sample_buff_in_index;
extern uint32_t g_sample_buff_out_index;

extern bool g_sample_buff_full;


void StartAcquisition ( void );
void StopAcquisition( void );

void init_adc(void);
void init_dma(void);

#endif
