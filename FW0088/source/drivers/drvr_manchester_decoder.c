//*****************************************************************************
//
// Include
//
//*****************************************************************************

// Standard Library Includes
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>

// TivaWare includes
#include "inc/hw_memmap.h"

// TivaWare driverlib
#include "driverlib/gpio.h"
#include "driverlib/sysctl.h"

// LWIP


// FATFs


// Include the text replace for hardware definitions


// Include the Firmware details file
#include "hardware.h"

// Must be called before micro SD


// Instrumentel HW Layer


// Instrumentel Drivers
#include "drivers/drvr_pin_state_decoder.h"
#include "drivers/drvr_manchester_decoder.h"
#include "drivers/drvr_debug_sw.h"

// Instrumentel Application



//****************************************************************************
//
// Prototypes
//
//****************************************************************************

bool check_within_tolerance(uint8_t a_expected_dur, uint8_t a_measured_dur, uint8_t a_tolerance_upper, uint8_t a_tolerance_lower);
uint16_t fletcher16( uint8_t const *data, size_t bytes );
void reset_tolerances(void);
void adjust_tolerances(void);
void debug_fault(FaultCodes a_fault);
void add_stats(uint8_t a_index, uint32_t a_measured_dur);

void trace_state_machine(EnvelopeDecoder a_env_state, Pin_states_t a_dir, uint16_t a_dur);
void tune_pulse_width(void);

//****************************************************************************
//
// Global Variables
//
//****************************************************************************

// The buffer we put data into
uint8_t g_rx_buff[100];
uint8_t g_rx_buff_holding[30];
bool g_envelope_data_available;

// State machine controller
EnvelopeDecoder g_env_state = STATE_IDLE;

//****************************************************************************
//
// Local Variables
//
//****************************************************************************

// State machine variables
static uint8_t last_state = 255;
static bool new_state = false;

// Pulse types
PulseTypes pulse_types[4] = 
{
    {"SS\0", 0, 0, 0, 0},
    {"SL\0", 0, 0, 0, 0},
    {"LS\0", 0, 0, 0, 0},
    {"LL\0", 0, 0, 0, 0}
};

// Preamble variables
uint8_t preamble_len = 6;
uint8_t preamble_index;

PulseProfile preamble[6] = {
    {HIGH, INDEX_PULSE, 20, 20}, 
    {LOW, LONG_LONG_PULSE, LONG_LONG_TOLERANCE_UPPER, LONG_LONG_TOLERANCE_LOWER}, 
    {HIGH, LONG_SHORT_PULSE, LONG_SHORT_TOLERANCE, LONG_SHORT_TOLERANCE},
    {LOW, SHORT_SHORT_PULSE, SHORT_SHORT_TOLERANCE, SHORT_SHORT_TOLERANCE}, 
    {HIGH, SHORT_SHORT_PULSE, SHORT_SHORT_TOLERANCE, SHORT_SHORT_TOLERANCE}, 
    {LOW, SHORT_SHORT_PULSE, SHORT_SHORT_TOLERANCE, SHORT_SHORT_TOLERANCE}
};

Pulse rx_preamble[8];
uint32_t rx_preamble_index = 0;

// Manchester data decoder variables
bool clocked;

// Byte position decoder vars
uint8_t rx_data_index = 0;
uint8_t rx_bit_index = 7;

// Counter for how long a pause has occurred for
uint16_t pause_duration = 0;

// Variable to track what kind of pulse we had for the last pulse
uint8_t last_pulse_type = 0;
uint8_t last_pulse_dur = 0;

// Track our pulse and state machine to debug firmware behaviour
EnvelopePulseDebug pulses[DEBUG_PULSES_BUFF_LEN];
uint8_t _pulses_index = 0;

uint32_t lost_data;

// An array of the types of manchester encoded pulses
PulseType g_manc_pulse_types[4] = 
{
    {
        SHORT_SHORT_PULSE,
        SHORT_SHORT_PULSE,
        SHORT_SHORT_TOLERANCE,
        SHORT_SHORT_TOLERANCE
    },
    {
        LONG_SHORT_PULSE,
        LONG_SHORT_PULSE,
        LONG_SHORT_TOLERANCE,
        LONG_SHORT_TOLERANCE
    },
    {
        SHORT_LONG_PULSE,
        SHORT_LONG_PULSE,
        SHORT_LONG_TOLERANCE_UPPER,
        SHORT_LONG_TOLERANCE_LOWER
    },
    {
        LONG_LONG_PULSE,
        LONG_LONG_PULSE,
        LONG_LONG_TOLERANCE_UPPER,
        LONG_LONG_TOLERANCE_LOWER
    }
};

PulseStats g_stats[4] = {
    { 0, 0, 0xFFFF, 0 }, 
    { 0, 0, 0xFFFF, 0 }, 
    { 0, 0, 0xFFFF, 0 }, 
    { 0, 0, 0xFFFF, 0 }
};

bool g_tuned;

extern bool g_adc_dump;


//****************************************************************************
//
// Global Functions
//
//****************************************************************************

void envelope_rx_process(Pin_states_t a_dir, uint16_t a_dur)
//**********************************************************
// Desc: This function is called within the background task and will check if we
//       have new data in the buffer to check. If we don't then jump out the 
//       routine.
//       If we have data, then look to see if it is matched the preamble pattern
//       If it does then decode the data and check it against the fletcher 16 cs
// Inputs: The pin's last state and how long it was in that state for.
// Return: None
{
    bool _expected_dur = false;
    bool _short_pulse, _long_pulse;
    uint16_t _calc_checksum, _rx_checksum;
    
    
        
//    GPIOPinWrite(LED_BASE, LED_4, 0);
    
    // Debug State Machine Variables
    //trace_state_machine(g_env_state, a_dir, a_dur);
    
  
    // Check whether we have just changes states
    if (g_env_state != last_state)
    {
        new_state = true;
        last_state = g_env_state;
    }
            
    // Run the state machine
    switch (g_env_state)
    {
        case STATE_IDLE:
            // Sit in this state and do nothing. Only transition out of here is if the pin goes mid range.
            if (a_dir == PIN_MID_RANGE)
            {
                debugf(DEBUG_GENERAL, "Enter pause state.");
                g_env_state = STATE_PAUSE;
            }
            //break;
            
        case STATE_PAUSE:
            
            if (new_state)
            {              
                debugf(DEBUG_MANC, "Enter pause state.");
                pause_duration = 0;
            }
            
            // While looking for a pause, the pin can bounce around so look to 
            // take it into account.
            pause_duration += a_dur;

            if (pause_duration > PULSE_DURATION)
            {
                if (a_dir == PIN_MID_RANGE)
                {
                    debugf(DEBUG_GENERAL, "Pause of %d ticks > required %d ticks.", pause_duration, PULSE_DURATION);
                    g_env_state = STATE_PREAMBLE;
                }
                else
                {
                    // We had multiple noise signals so go back to IDLE
                    g_env_state = STATE_PAUSE_LOST;
                }
            }
            break;

        case STATE_PREAMBLE:
            if (new_state)
            {
                preamble_index = 0;
                
                rx_preamble_index = 0;
            }
            
            // New code
            
            // Add the preamble pulse to the preamble array.
            rx_preamble[rx_preamble_index].dir = a_dir;
            rx_preamble[rx_preamble_index].dur = a_dur;
            rx_preamble_index++;
            
            if (rx_preamble_index == 6)
            {
                for (int x = 0; x < rx_preamble_index; x++)
                {
                    debugf(DEBUG_MANC, "Preamble[%d], dir: %d, dur: %d", x, rx_preamble[x].dir, rx_preamble[x].dur);
                }
                
                tune_pulse_width();
                
                // Move to the next state
                g_env_state = STATE_DATA;
            }
            
            // New code End
            #if 0
            //debugf(DEBUG_MANC, "PREAMBLE. Index: %d, Expected: %d, Upper: %d, Lower: %d", preamble_index, preamble[preamble_index].dur, preamble[preamble_index].upper_tol, preamble[preamble_index].lower_tol);
            _expected_dur = check_within_tolerance(preamble[preamble_index].dur, a_dur, preamble[preamble_index].upper_tol, preamble[preamble_index].lower_tol);

            // Check the data against the preamble
            if ((preamble[preamble_index].dir == a_dir) && _expected_dur)
            {              
                // Found a preamble bit! Look for the next one
                preamble_index += 1;
            }
            else
            {
                #if 0
                trace_pulse(a_dir, a_dur);
                #endif

                // Can't find the preamble bit, go back to the start
                g_env_state = STATE_PREAMBLE_LOST;
            }

            if (preamble_index == preamble_len)
            {
                // Found the full preamble. 
                debugf(DEBUG_MANC, "Found full preamble so look for data.");
                g_env_state = STATE_DATA;
            }
            #endif
            break;
            
        case STATE_DATA:
            
            if (new_state)
            {
                debugf(DEBUG_MANC, "DATA");
                                
                rx_bit_index = 7;
                rx_data_index = 1;
                
                memset(g_rx_buff, 0x00, sizeof(g_rx_buff));
                clocked = false;
                
                // The last pulse from the preamble will always bee short
                last_pulse_type = SHORT_PULSE;
            }
            
            // Our tolerances and pulse durations change based on the previous pulse we last had.
            if (last_pulse_type == SHORT_PULSE)
            {
                _long_pulse = check_within_tolerance(pulse_types[SHORT_LONG_INDEX].duration, a_dur, pulse_types[SHORT_LONG_INDEX].upper_tol, pulse_types[SHORT_LONG_INDEX].lower_tol);
                _short_pulse = check_within_tolerance(pulse_types[SHORT_SHORT_INDEX].duration, a_dur, pulse_types[SHORT_SHORT_INDEX].upper_tol, pulse_types[SHORT_SHORT_INDEX].lower_tol);
            }
            else if (last_pulse_type == LONG_PULSE)
            {
                _short_pulse = check_within_tolerance(pulse_types[LONG_SHORT_INDEX].duration, a_dur, pulse_types[LONG_SHORT_INDEX].upper_tol, pulse_types[LONG_SHORT_INDEX].lower_tol);
                _long_pulse = check_within_tolerance(pulse_types[LONG_LONG_INDEX].duration, a_dur, pulse_types[LONG_LONG_INDEX].upper_tol, pulse_types[LONG_LONG_INDEX].lower_tol);
            }
            
            // Decode the data ready to be output on UART.
            if (_short_pulse && !clocked)
            {        
                last_pulse_type = SHORT_PULSE;
                
                // The pin has change state on a clock
                clocked = true;
            }
            else if (_short_pulse && clocked)
            {         
                last_pulse_type = SHORT_PULSE;
                
                clocked = false;

                g_rx_buff[rx_data_index] |= a_dir << rx_bit_index;

                // update the bit/byte index tracker
                if (rx_bit_index-- == 0)
                {
                    debugf(DEBUG_MANC, "Databyte [%d]: %X", rx_data_index, g_rx_buff[rx_data_index]);
                    
                    rx_bit_index = 7;
                    rx_data_index += 1;
                }
            }
            else if (_long_pulse)
            {          
                last_pulse_type = LONG_PULSE;
                
                // The data has change state from 1 to 0 or vice versa, therefore a clock wasn't needed.
                clocked = false;

                g_rx_buff[rx_data_index] |= a_dir << rx_bit_index;

                // update the bit/byte index tracker
                if (rx_bit_index-- == 0)
                {
                    debugf(DEBUG_MANC, "Databyte [%d]: %X", rx_data_index, g_rx_buff[rx_data_index]);
                    
                    rx_bit_index = 7;
                    rx_data_index += 1;
                }
            }
            else
            {    
                #if 0                
                trace_pulse(a_dir, a_dur);
                #endif
                
                debugf(DEBUG_MANC, "Pulse lost:  dir: %d, dur: %d", a_dir, a_dur);
                g_env_state = STATE_DATA_LOST;
            }
            
            // We have 17 bytes of data
            if (rx_data_index == (DATA_LEN + CS_LEN + 1)) // Add 2 for checksum bytes
            {
                _calc_checksum = fletcher16((const uint8_t*)&g_rx_buff[1], 14);
                    
                _rx_checksum = ((g_rx_buff[DATA_LEN] << 8) + g_rx_buff[DATA_LEN+1]);
                
                if (_calc_checksum != _rx_checksum)
                {                    
                    debugf(DEBUG_GENERAL, "_calc_checksum: %X, _rx_checksum: %X", _calc_checksum, _rx_checksum);

                    // We have all the data so jump to the preamble search
                    g_env_state = STATE_CS_FAIL;
                }
                else
                {
                    debugf(DEBUG_GENERAL, "Checksum passed! %X", _calc_checksum);
                    lost_data = 0;
                    
                    adjust_tolerances();
                    
                    memcpy(g_rx_buff_holding, g_rx_buff, sizeof(g_rx_buff_holding));
                    g_rx_buff[0] = 0;
                    g_envelope_data_available = true;
                    
                    // We have all the data so jump to the preamble search
                    g_env_state = STATE_IDLE;
                }
            }
            break;
            
        case STATE_PAUSE_LOST:
            if (new_state)
            {                
                debugf(DEBUG_GENERAL, "STATE_PAUSE_LOST");
                debug_fault(PAUSE_LOST);
    
                #ifdef DEBUG_LOST_PAUSE
                    PORTBbits.RB2 = 0;
                #endif
            }
            
            // Return to the IDLE state and look for a pause
            g_env_state = STATE_IDLE;
            break;
            
        case STATE_PREAMBLE_LOST:
            if (new_state)
            {      
                debugf(DEBUG_GENERAL, "STATE_PREAMBLE_LOST");
                
                debug_fault(PREAMBLE_LOST);
                g_rx_buff[0] = 1;
                
                g_envelope_data_available = true;
                    
                #ifdef DEBUG_LOST_PREAMBLE
                    PORTBbits.RB2 = 0;
                #endif
            }
            
            // Return to the IDLE state and look for a pause
            g_env_state = STATE_IDLE;
            break;
            
        case STATE_DATA_LOST:
            if (new_state)
            {
                debugf(DEBUG_GENERAL, "STATE_DATA_LOST");

                lost_data += 1;
                
                // We have failed 10 times so retune the thresholds.
                if (lost_data == SUCCESSIVE_FAILURES)
                {
                    debugf(DEBUG_MANC, "lost_data = 5, reset_amplitude_thresholds");
                    reset_amplitude_thresholds();
                    lost_data = 0;
                }
                
                debug_fault(DATA_LOST);
                g_rx_buff[0] = 2;
                
                g_envelope_data_available = true;
                //g_adc_dump = true;
                
#ifdef DEBUG_LOST_DATA
                PORTBbits.RB8 = 0;
#endif
            }
            
            // Return to the IDLE state and look for a pause
            g_env_state = STATE_IDLE;
            break;
            
        case STATE_CS_FAIL:
            if (new_state)
            {            
                debugf(DEBUG_GENERAL, "STATE_CS_FAIL");
                
                lost_data += 1;
                
                // We have failed 10 times so retune the thresholds.
                if (lost_data == SUCCESSIVE_FAILURES)
                {
                    debugf(DEBUG_MANC, "lost_data = 5, reset_amplitude_thresholds");
                    reset_amplitude_thresholds();
                    lost_data = 0;
                }
                
                debug_fault(CHECKSUM_FAILED);
                g_rx_buff[0] = 3;
                
                g_envelope_data_available = true;
            }
            
            // Return to the IDLE state and look for a pause
            g_env_state = STATE_IDLE;
            break;
    }

    last_pulse_dur = a_dur;
    new_state = false;
    
//    GPIOPinWrite(LED_BASE, LED_4, LED_4);
}


//****************************************************************************
//
// Local Functions
//
//****************************************************************************

bool check_within_tolerance(uint8_t a_expected_dur, uint8_t a_measured_dur, uint8_t a_tolerance_upper, uint8_t a_tolerance_lower)
//**********************************************************************************************
// Desc: Take the measured and expected values with the upper and lower tolerance to determin 
// Inputs: None
// Return: None
{
    uint8_t _index = 4;
    
    // If the measure signal is within the bounds
    if ((a_measured_dur >= (a_expected_dur - a_tolerance_lower)) && (a_measured_dur <= (a_expected_dur + a_tolerance_upper)))
    {
        // Check if the expected duration is SHORT-SHORT
        if (a_expected_dur == g_manc_pulse_types[0].tuned_dur)
        {
            _index = 0;
        }
        else if (a_expected_dur == g_manc_pulse_types[1].tuned_dur)
        {
            _index = 1;
        }
        else if (a_expected_dur == g_manc_pulse_types[2].tuned_dur)
        {
            _index = 2;
        }
        // Check if the expected duration is LONG-LONG
        else if (a_expected_dur == g_manc_pulse_types[3].tuned_dur)
        {
            _index = 3;
        }
        else
        {
            _index = 4;
        }
        
        // Only add to stats if we found how to index the pulse type
        if (_index < 4)
        {
            add_stats(_index, a_measured_dur);
        }
        
        return true;
    }
    
    return false;
}

uint16_t fletcher16( uint8_t const *data, size_t bytes )
//******************************************************
// Desc: Calculate the fletcher 16 on a stream of data bytes.
// Inputs: Pointer to string of data and how long the string is.
// Return: fletcher 16 checksum
{
    uint16_t sum1 = 0xff, sum2 = 0xff;
    
    while (bytes) 
    {
        size_t tlen = bytes > 20 ? 20 : bytes;
        bytes -= tlen;

        do 
        {
            sum2 += sum1 += *data++;
        } 
        while (--tlen);

        sum1 = (sum1 & 0xff) + (sum1 >> 8);
        sum2 = (sum2 & 0xff) + (sum2 >> 8);
    }
    
    /* Second reduction step to reduce sums to 8 bits */
    sum1 = (sum1 & 0xff) + (sum1 >> 8);
    sum2 = (sum2 & 0xff) + (sum2 >> 8);
    
    return sum2 << 8 | sum1;
}

void reset_tolerances(void)
//*************************
// Desc: We've adjusted the tolerances but keep getting repeated checksum failure so reset them
// Inputs: None
// Return: None
{    
#if 0
    g_manc_pulse_types = 
    {
        {
            SHORT_SHORT_PULSE,
            SHORT_SHORT_PULSE,
            SHORT_SHORT_TOLERANCE,
            SHORT_SHORT_TOLERANCE
        },
        {
            LONG_SHORT_PULSE,
            LONG_SHORT_PULSE,
            LONG_SHORT_TOLERANCE,
            LONG_SHORT_TOLERANCE
        },
        {
            SHORT_LONG_PULSE,
            SHORT_LONG_PULSE,
            SHORT_LONG_TOLERANCE_UPPER,
            SHORT_LONG_TOLERANCE_LOWER
        },
        {
            LONG_LONG_PULSE,
            LONG_LONG_PULSE,
            LONG_LONG_TOLERANCE_UPPER,
            LONG_LONG_TOLERANCE_LOWER
        }
    };
#endif
}

void add_stats(uint8_t a_index, uint32_t a_measured_dur)
{
    //debugf(DEBUG_MANC, "Update %d stats", a_index);
    g_stats[a_index].total += a_measured_dur;
    g_stats[a_index].count += 1;
    
    #if 0
    uart_send_string("\n index: ");
    uart_send_string(itoa(a_index));
    uart_send_string(",total: ");
    uart_send_string(itoa(g_stats[a_index].total));
    uart_send_string(", count: ");
    uart_send_string(itoa(g_stats[a_index].count));
    #endif

    // If we have a new min
    if (a_measured_dur < g_stats[a_index].min)
    {
        g_stats[a_index].min = a_measured_dur;
    }

    // If we have a new max value
    if (a_measured_dur > g_stats[a_index].max)
    {
        g_stats[a_index].max = a_measured_dur;
    }
}

void adjust_tolerances(void)
//**************************
// Desc: 
// Inputs: 
// Return: 
{
    uint8_t x;
    int8_t _diff, _min_hys, _max_hys;
    debugf(DEBUG_MANC, "adjust_tolerances");
    
    uint8_t _short_pulse_thres;
    uint8_t _long_pulse_thres;
    
    // If we need to adjust the limits then make sure the windows for long pulse and short pulse detection cannot cross over
    if ((g_manc_pulse_types[0].upper_tol + g_manc_pulse_types[0].tuned_dur) > (g_manc_pulse_types[1].upper_tol + g_manc_pulse_types[1].tuned_dur))
    {  
        _short_pulse_thres = g_manc_pulse_types[0].upper_tol + g_manc_pulse_types[0].tuned_dur;
    }
    else
    {
        _short_pulse_thres = g_manc_pulse_types[1].upper_tol + g_manc_pulse_types[1].tuned_dur;
    }
    
    if ((g_manc_pulse_types[2].tuned_dur - g_manc_pulse_types[2].lower_tol) > (g_manc_pulse_types[3].tuned_dur - g_manc_pulse_types[3].lower_tol))
    {  
        _long_pulse_thres = g_manc_pulse_types[2].tuned_dur - g_manc_pulse_types[2].lower_tol;
    }
    else
    {
        _long_pulse_thres = g_manc_pulse_types[3].tuned_dur - g_manc_pulse_types[3].lower_tol;
    }
    
    for (x=0; x<4; x++)
    {
        if (g_stats[x].count == 0)
        {
            // No pulses to gather stats so skip
            debugf(DEBUG_MANC, "skip");
            continue;
        }
                
        uint32_t _average = g_stats[x].total / g_stats[x].count;
        
        // Find the difference between the average and the tuned 
        _diff = _average - g_manc_pulse_types[x].tuned_dur;
        
        // Find the hystersis on the min and max values to see if we are getting close to the limits.
        _min_hys = g_stats[x].min - (g_manc_pulse_types[x].tuned_dur - g_manc_pulse_types[x].lower_tol);
        _max_hys = (g_manc_pulse_types[x].tuned_dur + g_manc_pulse_types[x].upper_tol) - g_stats[x].max;
        
        
        debugf(DEBUG_MANC, "Index: %d, tuned: %d, upper: %d, lower: %d", x, g_manc_pulse_types[x].tuned_dur, 
            g_manc_pulse_types[x].tuned_dur + g_manc_pulse_types[x].upper_tol,
            g_manc_pulse_types[x].tuned_dur - g_manc_pulse_types[x].lower_tol);
        
        debugf(DEBUG_MANC, "Av: %d, total: %d, count: %d, diff: %d", x, _average, 
            g_stats[x].total,
            g_stats[x].count,
            _diff);
        
        debugf(DEBUG_MANC, "Min: %d, Min hys: %d", g_stats[x].min, _min_hys);
        debugf(DEBUG_MANC, "Max: %d, Max hys: %d", g_stats[x].max, _max_hys);
        
        #if 0
        uart_send_string("\nIndex: ");
        uart_send_string(itoa(x));
        uart_send_string(", tuned: ");
        uart_send_string(itoa(g_manc_pulse_types[x].tuned_dur));
        uart_send_string(", upper: ");
        uart_send_string(itoa(g_manc_pulse_types[x].tuned_dur + g_manc_pulse_types[x].upper_tol));
        uart_send_string(", lower: ");
        uart_send_string(itoa(g_manc_pulse_types[x].tuned_dur - g_manc_pulse_types[x].lower_tol));
        uart_send_string(", average: ");
        uart_send_string(itoa(_average));
        uart_send_string(", total: ");
        uart_send_string(itoa(g_stats[x].total));
        uart_send_string(", count: ");
        uart_send_string(itoa(g_stats[x].count));
        uart_send_string(", min: ");
        uart_send_string(itoa(g_stats[x].min));
        uart_send_string(", min hys: ");
        uart_send_string(itoa(_min_hys));
        uart_send_string(", max: ");
        uart_send_string(itoa(g_stats[x].max));
        uart_send_string(", max hys: ");
        uart_send_string(itoa(_max_hys));
        uart_send_string(", _diff: ");
        uart_send_string(itoa(_diff));
        #endif
        
        // Reset the stat vars
        g_stats[x].total = 0;
        g_stats[x].count = 0;
        g_stats[x].min = 0xFFFF;
        g_stats[x].max = 0;
        
        // Adjust the detection windows
        if ((_diff > 2) || (_diff < -2))
        {
            debugf(DEBUG_MANC, "Diff too high. Adjust.");
            g_tuned = true;
            g_manc_pulse_types[x].tuned_dur += _diff;            
        }
        else if (_max_hys < 2)
        {
            debugf(DEBUG_MANC, "_max_hys too low. Adjust.\n");
            g_tuned = true;
            g_manc_pulse_types[x].tuned_dur += 1;
        }
        else if (_min_hys < 2)
        {
            debugf(DEBUG_MANC, "_min_hys too low. Adjust.\n");
            g_tuned = true;
            g_manc_pulse_types[x].tuned_dur -= 1;
        }
        
        // Short pulse types
        // When we adjust SHORT SHORT or LONG SHORT, we must ensure the max value does not cross into the min value for LONG SHORT and LONG LONG
        if ((x == 0) || (x == 1))
        {
            // Check the tuned value doesnt cross the threshold
            if ((g_manc_pulse_types[x].tuned_dur + g_manc_pulse_types[x].upper_tol) > _long_pulse_thres)
            {
                #if 0
                uart_send_string("\nClose to limit. Adjust short pulse.");
                uart_send_string("\nPulse upper threshold: ");
                uart_send_string(itoa((g_manc_pulse_types[x].tuned_dur + g_manc_pulse_types[x].upper_tol)));
                uart_send_string("\nLong pulse lower threshold:  ");
                uart_send_string(itoa(_long_pulse_thres));
                #endif
                
                // We've overtuned and moved a short pulse into the long pulse window
                g_manc_pulse_types[x].upper_tol = _long_pulse_thres - g_manc_pulse_types[x].tuned_dur;
            }
        }
        // Long pulse types
        // When we adjust LONG SHORT and LONG LONG, we must ensure the min value does not cross into the max value for SHORT SHORT or LONG SHORT
        else if ((x == 2) || (x == 3))
        {
            // Check the tuned value doesnt cross the threshold limit
            if ((g_manc_pulse_types[x].tuned_dur - g_manc_pulse_types[x].lower_tol) < _short_pulse_thres)
            {
                debugf(DEBUG_MANC, "Close to limit. Adjust long pulse.");
                
                // We've overtuned and moved a short pulse into the long pulse window
                g_manc_pulse_types[x].lower_tol = g_manc_pulse_types[x].tuned_dur - _short_pulse_thres;
            }
        }
    }
}


void debug_fault(FaultCodes a_fault)
//**********************************
// Desc: Trace out the pulse behaviour to debug faults.
// Inputs: Fault type
// Return: None
{
#ifdef DEBUG_SIGNALS
    
    uint8_t x;
    int16_t _index = 0;

    print_pin_stats();
    reset_pin_stats();
    
    uart_send_string("\n");
    
    // Limit the debug output
    for (x=0; x<DEBUG_PULSES_BUFF_LEN/4; x++)
    {
        _index = _pulses_index - x;
        
        if (_index < 0)
        {
            _index += DEBUG_PULSES_BUFF_LEN;
        }
        
        uart_send_string("Pulses[");
        uart_send_string(itoa(_index));
        uart_send_string("] ");
        
        // Check when we need to stop outputting pulses
        switch(pulses[_index].state)
        {
            case STATE_IDLE: uart_send_string("IDLE - "); break;
            case STATE_PAUSE: uart_send_string("PAUSE - "); break;
            case STATE_PREAMBLE: uart_send_string("PREAMBLE - "); break;
            case STATE_DATA: uart_send_string("DATA - "); break;
            case STATE_PAUSE_LOST: uart_send_string("FAULT PAUSE - "); break;
            case STATE_PREAMBLE_LOST: uart_send_string("FAULT PRE - "); break;
            case STATE_DATA_LOST: uart_send_string("FAULT DATA - "); break;
            case STATE_CS_FAIL: uart_send_string("FAULT CS - "); break;
            
        }
        
        uart_send_string("dir: ");
        uart_send_string(itoa(pulses[_index].dir));
        uart_send_string(", dur: ");
        uart_send_string(itoa(pulses[_index].dur));
        uart_send_string("\n");
        
        // Don't exit out before tracing out at least 2 pulses
        if (x < 2)
        {
            continue;
        }
        
        // Check when we need to stop outputting pulses
        switch(a_fault)
        {
            case PAUSE_LOST:
                if ((pulses[_index].state == STATE_DATA) || (pulses[_index].state == STATE_PREAMBLE) || 
                    (pulses[_index].state == STATE_PREAMBLE_LOST) || 
                    (pulses[_index].state == STATE_DATA_LOST) || (pulses[_index].state == STATE_CS_FAIL) || 
                    (pulses[_index].state == STATE_IDLE))
                {
                    // We're back to where we were in the IDLE state so exit.
                    return;
                }
                break;
                
            case PREAMBLE_LOST:
                if ((pulses[_index].state == STATE_DATA) || (pulses[_index].state == STATE_PREAMBLE_LOST) || 
                    (pulses[_index].state == STATE_DATA_LOST) || (pulses[_index].state == STATE_CS_FAIL) || 
                    (pulses[_index].state == STATE_IDLE))
                {
                    // We're back to where we were in the IDLE state so exit.
                    return;
                }
                break;
                
            case DATA_LOST:
                break;
                
            case CHECKSUM_FAILED:
                break;
                
            default:
                break;
        }
    }
    
#endif
}

void trace_state_machine(EnvelopeDecoder a_env_state, Pin_states_t a_dir, uint16_t a_dur)
{
    static char _str[100]; 
    
    if ((STATE_DATA == a_env_state) || (STATE_PAUSE_LOST == a_env_state) || (STATE_DATA_LOST == a_env_state))
    {
        return;
    }
    
    memset(_str, 0x00, sizeof(_str));
    
    switch (a_env_state)
    {
        case STATE_IDLE:                strcat(_str, "IDLE.");                  break;
        case STATE_PAUSE:               strcat(_str, "PAUSE.");                 break;
        case STATE_PREAMBLE:            strcat(_str, "PREAMBLE.");              break;
        case STATE_DATA:                strcat(_str, "DATA.");                  break;
        case STATE_PAUSE_LOST:          strcat(_str, "LOST_PAUSE.");            break;
        case STATE_PREAMBLE_LOST:       strcat(_str, "LOST_PRAMBLE.");          break;
        case STATE_DATA_LOST:           strcat(_str, "LOST_DATA.");             break;
        case STATE_CS_FAIL:             strcat(_str, "CS_FAIL.");               break;
    }
    
    switch (a_dir)
    {
        case PIN_LOW:                   strcat(_str, " LOW,");                  break;
        case PIN_HIGH:                  strcat(_str, " HIGH,");                 break;
        case PIN_MID_RANGE:             strcat(_str, " MID RANGE,");            break;
    }
    
    debugf(DEBUG_MANC, "%s dur: %d", _str, a_dur);
}

void tune_pulse_width(void)
//*************************
// Desc: 
// Inputs: 
// Return: 
{    
    // Clear the decode buffer for each message
    memset(pulse_types, 0x00, sizeof(pulse_types));
    
    memcpy(pulse_types[SHORT_SHORT_INDEX].type , "SS\0", 3);
    memcpy(pulse_types[SHORT_LONG_INDEX].type , "SL\0", 3);
    memcpy(pulse_types[LONG_SHORT_INDEX].type , "LS\0", 3);
    memcpy(pulse_types[LONG_LONG_INDEX].type , "LL\0", 3);
    
    for (int x = 0; x < rx_preamble_index; x++)
    {
        switch (x)
        {
            case 0:
                // Preamble - SL
                pulse_types[SHORT_LONG_INDEX].duration += rx_preamble[x].dur;
                pulse_types[SHORT_LONG_INDEX].count += 1;
            
                // Average out the result
                pulse_types[SHORT_LONG_INDEX].duration /= pulse_types[SHORT_LONG_INDEX].count;
                pulse_types[SHORT_LONG_INDEX].count = 1;
            
                pulse_types[SHORT_LONG_INDEX].duration = 40;
                break;
            
            case 1:
                // Preamble - LL
                pulse_types[LONG_LONG_INDEX].duration += rx_preamble[x].dur;
                pulse_types[LONG_LONG_INDEX].count += 1;
            
                // Average out the result
                pulse_types[LONG_LONG_INDEX].duration /= pulse_types[LONG_LONG_INDEX].count;
                pulse_types[LONG_LONG_INDEX].count = 1;
            
                pulse_types[LONG_LONG_INDEX].duration = 40;
                break;
            
            case 2:
                // Preamble - LS
                pulse_types[LONG_SHORT_INDEX].duration += rx_preamble[x].dur;
                pulse_types[LONG_SHORT_INDEX].count += 1;
            
                // Average out the result
                pulse_types[LONG_SHORT_INDEX].duration /= pulse_types[LONG_SHORT_INDEX].count;
                pulse_types[LONG_SHORT_INDEX].count = 1;
            
                pulse_types[LONG_SHORT_INDEX].duration = 20;
                break;
            
            case 3:
            case 4:
            case 5:
                // Preamble - SS
                pulse_types[SHORT_SHORT_INDEX].duration += rx_preamble[x].dur;
                pulse_types[SHORT_SHORT_INDEX].count += 1;
            
                // Average out the result
                pulse_types[SHORT_SHORT_INDEX].duration /= pulse_types[SHORT_SHORT_INDEX].count;
                pulse_types[SHORT_SHORT_INDEX].count = 1;
            
                pulse_types[SHORT_SHORT_INDEX].duration = 20;
                break;
            
        }
    }
    
    // We've got the mean for our 4 pulse types so move onto the thresholds.
    int32_t _short_diff = pulse_types[SHORT_LONG_INDEX].duration - pulse_types[SHORT_SHORT_INDEX].duration;
    int32_t _long_diff = pulse_types[LONG_LONG_INDEX].duration - pulse_types[LONG_SHORT_INDEX].duration;
    
    // Lower Tolerances
    pulse_types[SHORT_SHORT_INDEX].lower_tol = (_short_diff / 2) - 1;
    pulse_types[SHORT_LONG_INDEX].lower_tol = (_short_diff / 2) - 1;
    pulse_types[LONG_SHORT_INDEX].lower_tol = (_long_diff / 2) - 1;
    pulse_types[LONG_LONG_INDEX].lower_tol = (_long_diff / 2) - 1;
    
    // Upper Tolerances
    pulse_types[SHORT_SHORT_INDEX].upper_tol = _short_diff / 2;
    pulse_types[SHORT_LONG_INDEX].upper_tol = _short_diff / 2;
    pulse_types[LONG_SHORT_INDEX].upper_tol = _long_diff / 2;
    pulse_types[LONG_LONG_INDEX].upper_tol = _long_diff / 2;
    
    debugf(DEBUG_MANC, "_short_diff: %d, _long_diff: %d", _short_diff, _long_diff);
    
    #if 0
    for (int x = 0; x < sizeof(pulse_types)/sizeof(PulseTypes); x++)
    {
        debugf(DEBUG_GENERAL, "Pulse type: %s, dur: %d, upper tol: %d, lower tol: %d", 
            pulse_types[x].type, pulse_types[x].duration, pulse_types[x].upper_tol, pulse_types[x].lower_tol);
        SysCtlDelay(0xFFF);
    }
    #endif
}
