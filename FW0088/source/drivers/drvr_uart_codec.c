

// message format {0xA5, 0x5A rest of payload}
// [ 0xA5,0X5A,<data length>,<tag ID>,<tag mode>,<packet count*>,<data>,<crc> ]

#include <stdint.h>
#include "drivers/drvr_uart_codec.h" 
#include "drivers/drvr_debug_sw.h"
//#include "application/app_uart_db_api.h"

#define UART_DEBUG

#define OutputDebugString(x) debugf(DEBUG_UART, x)
//#define OutputDebugString(x)

//#define UART_DEBUG

#define SYNCBYTE_FIRST 0xA5
#define SYNCBYTE_SECOND 0x5A
#define CRC16 0x8005 

UARTEncoder g_UARTMoboEncoder;
UARTDecoder g_UARTPythonDecoder;
char python_decoder_buff[100];
char mobo_buff[100];


static inline void UARTEncoder_SendData( UARTEncoder * uen, const uint8_t *data, uint32_t len);
static inline void send_preamble_wlen( UARTEncoder * uen , uint16_t len );
static inline void gencrc16_init(UARTCRC16 * uartcrc);
static inline void gencrc16_data(UARTCRC16 * uartcrc, const uint8_t *data, uint16_t size);
static inline uint16_t gencrc16_end(UARTCRC16 * uartcrc);

static inline void CommandProcess( UARTDecoder * ud );

static inline void UARTEncoder_SendData( UARTEncoder * uen, const uint8_t *data, uint32_t len)
{
    if (uen->sendcall != NULL)
        uen->sendcall(data, len, uen);
}

void UARTEncoder_Init( UARTEncoder * uen )
{
    uen->feature_flags = UARTDECODER_FLAGS_NONE;
    uen->sendcall = NULL;
    uen->tag = NULL;
    uen->is_stream = false;
}

void UARTEncoder_SetSendCall( UARTEncoder * uen, UARTEncoderCall call)
{
    uen->sendcall = call;
}

void UARTEncoder_SendPacket(  UARTEncoder * uen , const uint8_t *data, uint32_t len )
{
    static uint8_t crc[2];

    send_preamble_wlen(uen, len);
    
    // send payload
    UARTEncoder_SendData(uen, data, len);
    
    // send CRC
    if(uen->feature_flags & UARTENCODER_FLAGS_CRCENABLED)
    {
        uint16_t crc16 = UARTDecoder_gencrc16(data, len);
        crc[0] = crc16&0xFF;
        crc[1] = (crc16 >> 8)&0xFF;
        UARTEncoder_SendData(uen, crc, 2);
    }
    
}

void UARTEncoder_StreamPacketInit(  UARTEncoder * uen , uint32_t total_len )
{
    /*
    Stream packet is for when UART data need to be transmitted with broken up buffer
    maybe used to memcpy data into a continous section. Which may not be possible in 
    memory restrained or processor restrained systems
    */
    
    const static uint8_t preamble[2] = "\xA5\x5A";
    static uint8_t crc[2];
    
    uen->is_stream = true;
    
    uen->stream_bytes_rem = total_len;
    
    send_preamble_wlen(uen, total_len);
    
    if(total_len == 0 )
    {
        if(uen->feature_flags & UARTENCODER_FLAGS_CRCENABLED)
        {
            uint16_t crc16 = UARTDecoder_gencrc16(preamble, 0);
            crc[0] = crc16&0xFF;
            crc[1] = (crc16 >> 8)&0xFF;
            UARTEncoder_SendData(uen, crc, 2);
        }
        // stream is over
        uen->is_stream = false;
    }
    
    // Init CRC
    gencrc16_init(&(uen->stream_crc));
    
}

void UARTEncoder_StreamPacketData(  UARTEncoder * uen , const uint8_t *data, uint32_t len )
{
    static uint8_t crc[2];
    
    if(uen->is_stream == false)
        return; // ideally should be an error code
    
    if(uen->stream_bytes_rem < len)
        len = uen->stream_bytes_rem; // ideally another error
    
    // TX the data
    UARTEncoder_SendData(uen, data, len);
    
    // Update the CRC
    gencrc16_data(&(uen->stream_crc), data, len);
    
    // Decemenet the counter
    uen->stream_bytes_rem -= len;
    
    // Are we at the end?
    if(uen->stream_bytes_rem == 0)
    {
        // pop the CRC if needed...
        if(uen->feature_flags & UARTENCODER_FLAGS_CRCENABLED)
        {
            uint16_t crc16 = gencrc16_end(&(uen->stream_crc));
            crc[0] = crc16&0xFF;
            crc[1] = (crc16 >> 8)&0xFF;
            UARTEncoder_SendData(uen, crc, 2);
        }
        
        uen->is_stream = false; // stream fin!
    }
}

void UARTDecoder_Init( UARTDecoder * ud , uint8_t * buff, uint32_t bufflen)
{
    ud->call = UARTDECODER_NULL;
    ud->state = UARTDECODER_STATE_SYNCBYTEONE;
    ud->length = 0;
    ud->data = buff;
    ud->maxdatalen = bufflen;
    ud->checksum = 0;
    ud->feature_flags = UARTDECODER_FLAGS_CRCENABLED | UARTDECODER_FLAGS_TWOBYTLELEN;
    ud->data_available = false;
    
    UARTDecoder_ResetStats( ud );
    debugf(DEBUG_GENERAL, " ud %08X INIT\n", ud);
}

void UARTDecoder_ResetStats( UARTDecoder * ud )
{
    ud->stat_procmsgs = 0;
    ud->stat_crcfailures = 0;
    ud->stat_preamblemiss = 0;
    ud->stat_allrxbytes = 0;
}

void UARTDecoder_DecodeMessageBuff( UARTDecoder * ud , uint8_t * pBuf, uint32_t len )
{
    uint32_t i;
    
    for(i=0; i<len; i++)
    {
        UARTDecoder_DecodeMessage(ud, pBuf[i]);
    }
}

void UARTDecoder_DecodeMessage( UARTDecoder * ud , uint8_t newByte )
{
    uint16_t cs_msg;
    uint16_t tmp_incomming_cs;
    
    // Add 1 to the total bytes recieved, regardless of state
    ud->stat_allrxbytes += 1;
    
    switch( ud->state )
    {
        case UARTDECODER_STATE_SYNCBYTEONE:
            #ifdef UART_DEBUG
            OutputDebugString("S1");
            #endif
            if ( SYNCBYTE_FIRST == newByte )
            {
                ud->state = UARTDECODER_STATE_SYNCBYTETWO;
            }
            else
            {
                ud->stat_preamblemiss += 1;
                
                #ifdef UART_DEBUG
                OutputDebugString("failed");
                #endif
            }
            break;
        case UARTDECODER_STATE_SYNCBYTETWO:
            #ifdef UART_DEBUG
            OutputDebugString("SB2");
            #endif
            if ( SYNCBYTE_SECOND == newByte )
            {	
                ud->state = UARTDECODER_STATE_PAYLOADSIZE; 
            }
            else if ( SYNCBYTE_FIRST == newByte )
            {	
                // Reset back, the previous byte may of been in error.
                ud->stat_preamblemiss += 1;
                
                ud->state = UARTDECODER_STATE_SYNCBYTETWO; 
            }
            else
            {
                ud->stat_preamblemiss += 1;
                
                #ifdef UART_DEBUG
                OutputDebugString("Revert to SB1");
                #endif
                
                ud->state = UARTDECODER_STATE_SYNCBYTEONE;
            }
            break;
        case UARTDECODER_STATE_PAYLOADSIZE:
            //save this message lengthsize for use in UART_DATA
            ud->length = newByte;

            //initialise index into buffer for storing data bytes
            ud->dataindex = 0;
            #ifdef UART_DEBUG
            //sprintf( dbgStr, "Length: %d \n", gUART0PacketLength );
            //OutputDebugString( "Length: 0x%X", ud->length );
            //UART_TXformatHEX(ud->length);
            //OutputDebugString("\n");
				    
            #endif
            if(ud->length == 0 && (!(ud->feature_flags & UARTDECODER_FLAGS_TWOBYTLELEN)))
            {
                // one byte length mode, at size zero, goto CRC mode if crc flag enabled
                if(ud->feature_flags & UARTDECODER_FLAGS_CRCENABLED)
                {
                    ud->state = UARTDECODER_STATE_CHECKSUMA;
                }
                else
                {
                    CommandProcess(ud);
                    ud->state = UARTDECODER_STATE_SYNCBYTEONE;
                }

            }
            else if(ud->feature_flags & UARTDECODER_FLAGS_TWOBYTLELEN )
            {
                    // there is another length byte...
                    ud->state = UARTDECODER_STATE_PAYLOADSIZE_TWO;
            }
            else    
            {
                ud->state = UARTDECODER_STATE_UARTDATA;
            }
            break;
        case UARTDECODER_STATE_PAYLOADSIZE_TWO:
            // The second length byte is the upper 7 bits 
            ud->length = (newByte << 8) + ud->length;
            if(ud->length == 0 )
            {
                if(ud->feature_flags & UARTDECODER_FLAGS_CRCENABLED)
                {
                    ud->state = UARTDECODER_STATE_CHECKSUMA;
                }
                else
                {
                    CommandProcess(ud);
                    ud->state = UARTDECODER_STATE_SYNCBYTEONE;
                }

            }
            else
            {
                ud->state = UARTDECODER_STATE_UARTDATA;
            }
            break;
        
        case UARTDECODER_STATE_UARTDATA:
            // Save all the data into an array
            // store the current byte into preset packet data array
            
            
            // Only copy data if it's within size
            if(ud->dataindex < ud->maxdatalen)
                ud->data[ud->dataindex] = newByte;
            #ifdef UART_DEBUG
            // sprintf( dbgStr, "B %X at %u \n" , newByte, gUART0PacketDataIndex);
            //OutputStringUART0( dbgStr );
            //OutputDebugString( "B: 0x" );
            //UART_TXformatHEX(newByte);
            //OutputDebugString(" at 0x");
            //UART_TXformatHEX(gUART0PacketDataIndex);
            //OutputDebugString("\n");
            #endif
            
            if( ud->dataindex >= ud->length - 1)   // remove command byte
            {                                                       // from length
                #ifdef UART_DEBUG
                OutputDebugString("Data Fin");
                #endif
                //prepare for start of next packet
                if(ud->feature_flags & UARTDECODER_FLAGS_CRCENABLED)
                {
                    ud->state = UARTDECODER_STATE_CHECKSUMA;
                }
                else
                {
                    ud->state = UARTDECODER_STATE_SYNCBYTEONE;
                    //decode the data
                    CommandProcess(ud);  
                }
            }
            else
            {
                #ifdef UART_DEBUG
                OutputDebugString("inc pkt");
                #endif
                ud->dataindex += 1;
            }
            break;
            
        case UARTDECODER_STATE_CHECKSUMA:				
            //OutputStringUART0("UART_CHECKSUM \r");          
            ud->checksum = newByte&0xFF;
            ud->state = UARTDECODER_STATE_CHECKSUMB;
            break;
        
        case UARTDECODER_STATE_CHECKSUMB:
            // Append the second Byte, this code could be cleaner...
            tmp_incomming_cs = (newByte);
            tmp_incomming_cs <<= 8;
            tmp_incomming_cs &= 0xFF00;
            tmp_incomming_cs |= ud->checksum;
            ud->checksum = tmp_incomming_cs;
            
            //ud->checksum |= (newByte << 8);       
            cs_msg = UARTDecoder_gencrc16(ud->data, ud->dataindex+1);
            //sprintf(debugstr, "Expected CS: %04X \r", cs_msg);
            //OutputStringUART0(debugstr);
            
            if (cs_msg != ud->checksum)
            {
                // Checksum mismatch
                ud->stat_crcfailures += 1;
                OutputDebugString("RX message Checksum failed");
                ud->state = UARTDECODER_STATE_SYNCBYTEONE;
            }
            else 
            {
                // Just a CRC Pass
                ud->stat_procmsgs += 1;
                ud->data_available = true;
                OutputDebugString("RX message, us or BC");
                CommandProcess(ud); 
            }                
            
            
            ud->state = UARTDECODER_STATE_SYNCBYTEONE;
            break;
            
        default:
            #ifdef UART_DEBUG
            OutputDebugString("Undefined Message Type" );
            #endif
            // reset state machine
            ud->state = UARTDECODER_STATE_SYNCBYTEONE;
            break;
    }
}


void UARTDecoder_SetMessageCall(UARTDecoder * ud, UARTDecoderCall call)
{
    debugf(DEBUG_GENERAL, " ud %08X set call %08X\n", ud, call);
    ud->call = call;
}

static inline void CommandProcess( UARTDecoder * ud )
{
    
    // Set the flag
//    debugf(DEBUG_GENERAL, "Setting data_available -> true");
//    OutputDebugString("Setting dat_available -> true");
    ud->data_available = true;
    
    if (ud->call != UARTDECODER_NULL)
    {
        #ifdef UART_DEBUG
        OutputDebugString("Call CmdProc\n" );
        #endif
//        debugf(DEBUG_GENERAL, " ud %08X calling %08X\n", ud, ud->call);
        ud->call(ud->data, ud->length, ud);
    }
    else
    {
//         debugf(DEBUG_GENERAL, " ud %08X calling %08X - NULL\n", ud, ud->call);
        #ifdef UART_DEBUG
            OutputDebugString("Cmd Proc is null!\n" );
            #endif
    }
}

uint16_t UARTDecoder_gencrc16(const uint8_t *data, uint16_t size)
{
    uint16_t out = 0;
    int bits_read = 0, bit_flag;

    /* Sanity check: */
    if(data == NULL)
        return 0;

    while(size > 0)
    {
        bit_flag = out >> 15;

        /* Get next bit: */
        out <<= 1;
        out |= (*data >> bits_read) & 1; // item a) work from the least significant bits

        /* Increment bit counter: */
        bits_read++;
        if(bits_read > 7)
        {
            bits_read = 0;
            data++;
            size--;
        }

        /* Cycle check: */
        if(bit_flag)
            out ^= CRC16;

    }

    // item b) "push out" the last 16 bits
    uint16_t i;
    for (i = 0; i < 16; ++i) {
        bit_flag = out >> 15;
        out <<= 1;
        if(bit_flag)
            out ^= CRC16;
    }

    // item c) reverse the bits
    uint16_t crc = 0;
    i = 0x8000;
    int j = 0x0001;
    //int ii = 0x7FFF;
    for (; i != 0; i= i>>1, j <<= 1) {
        if (i & out) crc |= j;
    }

    return crc;
}

static inline void send_preamble_wlen( UARTEncoder * uen , uint16_t len )
{
    const static uint8_t preamble[2] = "\xA5\x5A";

    static uint8_t len_array[2], len_to_send;
    
    // send preamble
    UARTEncoder_SendData(uen, preamble, sizeof(preamble));
    len_to_send = 1;
    
    // send len
    if(uen->feature_flags & UARTENCODER_FLAGS_TWOBYTLELEN)
    {
        uint8_t first, second;
        
        first = (len & 0xFF);
        second = len >> 8; 
        len_to_send = 2;
        
        len_array[0] = first;
        len_array[1] = second;
    }
    else
    {
        len_array[0] = len;
        len_array[1] = 0;
    }
    
    UARTEncoder_SendData(uen, len_array, len_to_send);
    
}

static inline void gencrc16_init(UARTCRC16 * uartcrc)
{
    uartcrc->out = 0;
    uartcrc->bits_read = 0;
}

static inline void gencrc16_data(UARTCRC16 * uartcrc, const uint8_t *data, uint16_t size)
{
    int bit_flag;
    while(size > 0)
    {
        bit_flag = uartcrc->out >> 15;

        /* Get next bit: */
        uartcrc->out <<= 1;
        uartcrc->out |= (*data >> uartcrc->bits_read) & 1; // item a) work from the least significant bits

        /* Increment bit counter: */
        uartcrc->bits_read++;
        if(uartcrc->bits_read > 7)
        {
            uartcrc->bits_read = 0;
            data++;
            size--;
        }

        /* Cycle check: */
        if(bit_flag)
            uartcrc->out ^= CRC16;

    }
}

static inline uint16_t gencrc16_end(UARTCRC16 * uartcrc)
{
    // item b) "push out" the last 16 bits
    uint16_t i;
    int bit_flag;
    for (i = 0; i < 16; ++i) {
        bit_flag = uartcrc->out >> 15;
        uartcrc->out <<= 1;
        if(bit_flag)
            uartcrc->out ^= CRC16;
    }

    // item c) reverse the bits
    uint16_t crc = 0;
    i = 0x8000;
    int j = 0x0001;
    //int ii = 0x7FFF;
    for (; i != 0; i= i>>1, j <<= 1) {
        if (i & uartcrc->out) crc |= j;
    }

    return crc;
}
