//*****************************************************************************
//
// Include
//
//*****************************************************************************

// Standard Library Includes
#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>
#include <stdio.h>
#include <math.h>
#include <string.h>

// TivaWare includes
#include "inc/hw_memmap.h"

// TivaWare driverlib
#include "driverlib/gpio.h"

// LWIP


// FATFs


// Include the text replace for hardware definitions


// Include the Firmware details file
#include "hardware.h"

// Must be called before micro SD


// Instrumentel HW Layer


// Instrumentel Drivers
#include "drivers/drvr_pin_state_decoder.h"
#include "drivers/drvr_manchester_decoder.h"
#include "drivers/drvr_debug_sw.h"

// Instrumentel Application



//****************************************************************************
//
// Prototypes
//
//****************************************************************************

void reset_debouncer(void);
bool debouncer(uint8_t a_index);

// Functions to tune the amplification of the signal.
void tune_amplitude(Pin_states_t a_pin_state, uint8_t a_last_pin_state, uint16_t a_adc_val);
ADCStats gen_stats(void);
void update_steady_states(int32_t a_mean);


//****************************************************************************
//
// Global Variables
//
//****************************************************************************


//****************************************************************************
//
// Local Variables
//
//****************************************************************************


// The following variables are used to control the state machine
static Pin_states_t pin_state = PIN_MID_RANGE;
static uint8_t last_pin_state = 255;
static bool new_state;

// The following variables are used to track the state of the pin over time.
uint8_t debounce_counter[3];
uint16_t _time_between_pulses = 0;

// The following variables are used to calculate the correct amount of amplification for the pin states.
static uint16_t last_ten_samples[STATS_BUFF];
static uint8_t last_ten_samples_index;

static SteadyState steady_states[STEADY_STATE_BUFF];
static uint16_t steady_states_index;

// The following variables are the thresholds to determine when a pin changes state
static uint32_t upper_threshold = 0xFFF;
static uint32_t lower_threshold = 0;


//****************************************************************************
//
// Global Functions
//
//****************************************************************************

void check_pin_state(uint16_t a_adc_val)
//************************************
// Desc: Check whether the envelope Rx pin has changed state in three possible states.
// Inputs: ADC value we have measured
// Return: None
{    
    GPIOPinWrite(LED_BASE, LED_3, 0);
    
    //GPIOPinWrite(LED_BASE, LED_3, LED_3);
    //return;
            
    // Check if we need to adjust our tolerances
    tune_amplitude(pin_state, last_pin_state, a_adc_val);
        
    // Check which state the pin is in. If it is a new state then set the bool.
    if (pin_state != last_pin_state)
    {
        new_state = true;
        last_pin_state = pin_state;
    }
    
    _time_between_pulses += 1;
    
    
    //debugf(DEBUG_PIN_STATE, ".");
    
    switch (pin_state)
    {
        case PIN_MID_RANGE:
            if (new_state)
            {
                //debugf(DEBUG_PIN_STATE, "Mid Range. Upper: %d, lower: %d", upper_threshold, lower_threshold);
                
                reset_debouncer();
                _time_between_pulses = 0;
            }
                        
            // Check the 
            if (a_adc_val > upper_threshold)
            {
                // Debounce the signal
                if (debouncer(DEBOUNCE_HIGH))
                {
                    envelope_rx_process(pin_state, _time_between_pulses);
                    
                    pin_state = PIN_HIGH;
                }
            }
            else if (a_adc_val < lower_threshold)
            {
                // Debounce the signal
                if (debouncer(DEBOUNCE_LOW))
                {
                    envelope_rx_process(pin_state, _time_between_pulses);
                    
                    pin_state = PIN_LOW;
                }
            }
            else
            {
                reset_debouncer();
            }
            break;
         
        case PIN_HIGH:
            if (new_state)
            {
                //debugf(DEBUG_PIN_STATE, "High. Upper: %d, lower: %d", upper_threshold, lower_threshold);
                
                reset_debouncer();
                 _time_between_pulses = 0;
            }
            
            // Check if the line has started to IDLE prior to a preamble
            if ((g_env_state != STATE_DATA) && (a_adc_val < upper_threshold) && (a_adc_val > lower_threshold))
            {
                // Debounce the signal
                if (debouncer(DEBOUNCE_MID_RANGE))
                {
                    envelope_rx_process(pin_state, _time_between_pulses);
                    
                    pin_state = PIN_MID_RANGE;
                }
            }
            // Check if the line has gone low
            else if (a_adc_val < lower_threshold)
            {
                // Debounce the signal
                if (debouncer(DEBOUNCE_LOW))
                {
                    envelope_rx_process(pin_state, _time_between_pulses);
                    
                    pin_state = PIN_LOW;
                }
            }
            else
            {
                reset_debouncer();
            }
            break;
            
        case PIN_LOW:
            if (new_state)
            {
                //debugf(DEBUG_PIN_STATE, "Low. Upper: %d, lower: %d", upper_threshold, lower_threshold);
                
                reset_debouncer();
                _time_between_pulses = 0;
            }
            
            if ((g_env_state != STATE_DATA) && (a_adc_val < upper_threshold) && (a_adc_val > lower_threshold))
            {
                // Debounce the signal
                if (debouncer(DEBOUNCE_MID_RANGE))
                {
                    envelope_rx_process(pin_state, _time_between_pulses);
                    
                    pin_state = PIN_MID_RANGE;
                }
            }
            else if (a_adc_val > upper_threshold)
            {
                // Debounce the signal
                if (debouncer(DEBOUNCE_HIGH))
                {
                    envelope_rx_process(pin_state, _time_between_pulses);
                    
                    pin_state = PIN_HIGH;
                }
            }
            else
            {       
                reset_debouncer();
            }
            break;
    }
        
    new_state = false;
    
    GPIOPinWrite(LED_BASE, LED_3, LED_3);
}


//****************************************************************************
//
// Local Functions
//
//****************************************************************************

void reset_debouncer(void)
//*******************************
// Desc: We've had a full pin state change recognised so reset the debouncers
// Inputs: None
// Return: None
{
    uint8_t x;
    
    for (x = 0; x < sizeof(debounce_counter); x++)
    {
        debounce_counter[x] = 0;
    }
}

bool debouncer(uint8_t a_index)
//************************************
// Desc: We've got a state change on a pin so add it to the debouncer for that
//       index position. (LOW = 0, HIGH = 1, MID = 2) If the number of debounces
//       is above the threshold, then return true for a full confirmed state 
//       change of the pin.
// Inputs: Index position in debouncer array to update
// Return: Has the number of debounces passed the threshold
{    
    debounce_counter[a_index] += 1;
    
    if (debounce_counter[a_index] == DEBOUNCE)
    {
        // Reset all debouncers
        debounce_counter[a_index] = 0;
        return true;
    }
    else
    {
        return false;
    }
}

void tune_amplitude(Pin_states_t a_pin_state, uint8_t a_last_pin_state, uint16_t a_adc_val)
//**********************************************************************************************
// Desc: This function is responsible for all controls relating to the amplification threshold. It does the following;
//      - Have we gone 1000 ADC samples without a state change? If no then return.
//      - Start logging ADC samples into a buffer of the last 10 samples.
//      - Once we have 10 samples, gather the stats on the ADC over that period.
//      - If we have a low standard deviation on the last 10 ADC samples, then the signal is in a steady state.
//      - Look at other instances of steady states and decide if the we should add the new steady state to a current pool, or to create a new one.
//      - Once we have a set number of similar steady states found for 2 distinct amplification types, then pull them from the pool.
//      - Use the 2 pools to set the upper and lower tolerances and reset all our variables.
// Inputs: Current Pin State, Last Pin State, Current ADC value.
// Return: None
{
    static uint32_t ticks_since_state_change; 
    ADCStats _adc_stats;
    
    // Check if we have recorded a state change. If we have then skip out.
    if (a_pin_state != a_last_pin_state)
    {
        ticks_since_state_change = 0;
        return;
    }
    
    ticks_since_state_change += 1;
    
    // Check we have been in the same state for 1000 ADC samples
    if (ticks_since_state_change < 1000)
    {    
        return;
    }
    
    // Add the data to the stats tracker to see what the signal is doing.
    last_ten_samples[last_ten_samples_index] = a_adc_val;
    last_ten_samples_index += 1;
    
    // We've hit the end of our small buffer so begin to generate stats from it.
    if (last_ten_samples_index != STATS_BUFF)
    {
        // We still need to gather more data before we take any actions
        return;
    }
    
    // Reset the stat tracking indexer
    last_ten_samples_index = 0;
    
    // Calculate stats from the last 10 samples
    _adc_stats = gen_stats();
    
    // Have we got enough ADC samples to begin adding to the steady state pools?
    if (_adc_stats.stand_dev < ADC_STANDARD_DEVIATION_THRESHOLD)
    {
        //debugf(DEBUG_PIN_STATE, "Last 10 samples stable. mean: %d, var: %d, std: %d, index: %d", _adc_stats.mean, _adc_stats.vari, _adc_stats.stand_dev, steady_states_index);                
        update_steady_states(_adc_stats.mean);
    }

    // Check if we have hit the end of the steady state buffer.
    if (steady_states_index == (sizeof(steady_states)/sizeof(SteadyState)))
    {
        //debugf(DEBUG_PIN_STATE, "We've filled our steady state buffer but haven't actioned on it yet.");
        //debugf(DEBUG_PIN_STATE, "Reset the steady state buffer as we shouldn't hit the end like this..");
        
        // Clear all the steady state data
        memset(steady_states, 0x00, sizeof(steady_states));
        steady_states_index = 0;
    }
    
    uint16_t _amp_levels_found = 0;
    uint16_t _amp_levels[2] = {0};    
    
    // Loop over the steady state pools to determine if we can update our threshold values
    for (int y = 0; y < steady_states_index; y++)
    {
        if (steady_states[y].count >= 10)
        {
            _amp_levels[_amp_levels_found] = steady_states[y].mean;
            _amp_levels_found += 1;
        }
    }
            
    // Once we have 2 valid AMP levels, find the upper and lower and assign accordingly.
    if (_amp_levels_found == 2)
    {
        if (_amp_levels[0] > _amp_levels[1])
        {
            upper_threshold = _amp_levels[0] - ADC_THRESHOLD_HYSTERESIS;
            lower_threshold = _amp_levels[1] + ADC_THRESHOLD_HYSTERESIS;
        }
        else if (_amp_levels[0] < _amp_levels[1])
        {
            upper_threshold = _amp_levels[1] - ADC_THRESHOLD_HYSTERESIS;
            lower_threshold = _amp_levels[0] + ADC_THRESHOLD_HYSTERESIS;
        }
        
        debugf(DEBUG_GENERAL, "tune_amplitude, Upper: %d, Lower: %d", upper_threshold, lower_threshold);
        
        // Check the gap between the upper and lower thresholds make sense.
        if ((lower_threshold + ADC_MIN_AMPLIFICATION_DIFF) > upper_threshold)
        {
            debugf(DEBUG_PIN_STATE, "tune_amplitude, Lower and Upper thresholds are too tight. Back off and reattempt.");
            
            upper_threshold = 0xFFF;
            lower_threshold = 0;
        }
                
        // Dump some steady state tracing.
        for (int y = 0; y < steady_states_index; y++)
        {
            //debugf(DEBUG_PIN_STATE, "steady_states[%d].mean: %d, steady_states[%d].count: %d", y, steady_states[y].mean, y, steady_states[y].count);
        }
        
        // Clear all the steady state data
        memset(steady_states, 0x00, sizeof(steady_states));
        steady_states_index = 0;
        
        // Clear the last 10 sample buffer
        memset(last_ten_samples, 0x00, sizeof(last_ten_samples));
        last_ten_samples_index = 0;
                
        // Reset the tick counter to try decode some data with the new thresholds before attempting a retune.
        ticks_since_state_change = 0;
    }
}

void reset_amplitude_thresholds(void)
{
    upper_threshold = 0xFFF;
    lower_threshold = 0;
}

ADCStats gen_stats(void)
//******************
// Desc: This function will take the last 10 ADC samples and calculate the stats for them inc mean, variance and standard deviation.
//       If the standard deviation is below a threshold then the pin is in a 'steady state' and can be added to the pool of steady states.
// Inputs: None
// Return: None
{
    ADCStats last_ten_samples_stats;
    
    // Reset the stats.
    last_ten_samples_stats.total = 0;
    last_ten_samples_stats.count = 0;
    last_ten_samples_stats.min = 0xFFF;
    last_ten_samples_stats.max = 0;
    last_ten_samples_stats.mean = 0;
    last_ten_samples_stats.vari = 0;
    last_ten_samples_stats.stand_dev = 0;
    
    // Loop through the data to get the mean.
    for(int x = 0; x < STATS_BUFF; x++)
    {
        last_ten_samples_stats.total += last_ten_samples[x];
        last_ten_samples_stats.count += 1;
        
        // Update the min
        if (last_ten_samples[x] < last_ten_samples_stats.min)
        {
            last_ten_samples_stats.min = last_ten_samples[x];
        }
        
        // Update the max
        if (last_ten_samples[x] > last_ten_samples_stats.max)
        {
            last_ten_samples_stats.max = last_ten_samples[x];
        }
    }
    
    // Update the mean
    last_ten_samples_stats.mean = last_ten_samples_stats.total / last_ten_samples_stats.count;
    
    // Loop through the data to get the mean.
    for(int x = 0; x < STATS_BUFF; x++)
    {
        uint16_t _diff = pow(last_ten_samples[x] - last_ten_samples_stats.mean, 2);
        last_ten_samples_stats.vari += _diff;
    }
    
    // Calc the variance
    last_ten_samples_stats.vari /= last_ten_samples_stats.count;
    
    // Calc the standard deviation.
    last_ten_samples_stats.stand_dev = sqrt(last_ten_samples_stats.vari);
    
    return last_ten_samples_stats;
}

void update_steady_states(int32_t a_mean)
//****************************************
// Desc: This function will look at other occasions we identified as steady state and see where our new mean fits...
// Inputs: The mean of the steady state we have.
// Return: None
{
    bool new_steady_state = true;

    // Check if we have any other similar steady state readings...
    for (int y = 0; y < steady_states_index; y++)
    {
        int32_t _upper = steady_states[y].mean + ADC_STEP_NOISE;
        int32_t _lower = steady_states[y].mean - ADC_STEP_NOISE;
        
        
        if ((a_mean < _upper) && (a_mean > _lower))
        {
            steady_states[y].mean += a_mean;
            steady_states[y].mean /= 2;
            
            steady_states[y].count += 1;
            
            new_steady_state = false;
            break;
        }
    }
    
    // Can't find any similar readings so create a new log
    if (new_steady_state)
    {
        steady_states[steady_states_index].mean = a_mean;
        steady_states[steady_states_index].count = 1;
        steady_states_index += 1;
        
        if (steady_states_index == sizeof(steady_states)/sizeof(SteadyState))
        {
            debugf(DEBUG_GENERAL, "steady_states_index at limit. Reset");
            steady_states_index = 0;
        }
    }
}

