//*****************************************************************************
//
// Include
//
//*****************************************************************************

// Standard Library Includes
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <math.h>

// TivaWare includes
#include "inc/hw_memmap.h"

// TivaWare driverlib
#include "driverlib/sysctl.h"
#include "driverlib/gpio.h"
#include "driverlib/i2c.h"
#include "driverlib/pin_map.h"

// LWIP

// FATFs

// Include the text replace for hardware definitions
#include "hardware.h"

// Include the Firmware details file

// Must be called before micro SD

// Instrumentel HW Layer
#include "hw_layer/hw_uart.h"
#include "hw_layer/hw_eeprom.h"

// Instrumentel Drivers
//#include "drivers/UARTdecoder.h"
#include "drivers/drvr_Si570.h"
#include "drivers/drvr_debug_sw.h"

// Instrumentel Application
//#include "application/main.h"

//****************************************************************************
//
// Prototypes
//
//****************************************************************************


//****************************************************************************
//
// Global Variables
//
//****************************************************************************


//****************************************************************************
//
// Local Variables
//
//****************************************************************************

unsigned char INITIAL_HSDIV;
unsigned char INITIAL_N1;
unsigned long long INITIAL_RFREQ;

double FXTAL;
float FOUT0;

unsigned char Si570_Reg[6];
unsigned char HS_DIV[6] = {4, 5, 6, 7, 9, 11};

struct Si570_Params Si570Channels[NUM_CHANNELS];


//****************************************************************************
//
// Global Functions
//
//****************************************************************************

void init_Si570( void )
//*********************
// Desc: Initialises the peripheral where the Si570 is connected.
// Inputs: None
// Return: None
{
    // Enable the Si570 Module Peripheral
    SysCtlPeripheralEnable(C1_OE_PERIPH);
    GPIOPinTypeGPIOOutput(C1_OE_PORTBASE, C1_OE_PIN );
    
    // Power Down the Module After the Initialisation
    GPIOPinWrite(C1_OE_PORTBASE, C1_OE_PIN, PIN_LOW);	
          
    debugf(DEBUG_EEPROM, "Channel ID: %d", CN_CH1);
	ReadStartupConfigChannels(&Si570Channels[CN_CH1], CN_CH1);
	
	SysCtlDelay(100000);
    
    // CLK Freq is stored in KHz and divided by 100 to get it to MHz when written to FOUT var
    uint32_t _clk_freq = ReadFreqEEPROM(CN_CH1);
    
    if (_clk_freq == 0xFFFF)
    {
        debugf(DEBUG_EEPROM, "Clock Freq: 8 MHz");
        
        // Set to default to 8 MHz
        Si570Channels[CN_CH1].FOUT = 8;
        
        // Write to EEPROM a freq of 800 KHz
        WriteFreqEEPROM(800, CN_CH1);
    }
    else
    {
        debugf(DEBUG_EEPROM, "Clock Freq: %d MHz", _clk_freq / 100);
        
        Si570Channels[CN_CH1].FOUT = _clk_freq / 100; 
    }
        
	debugf(DEBUG_EEPROM, "Channel ID: %d", CN_CH1);
	ConfigOutputFreqChannels(&Si570Channels[CN_CH1], CN_CH1);
}

uint32_t I2CReceive(uint32_t channel, uint8_t reg)
//************************************************
// Desc: Receives data from the I2C channel where the slave is connected
// Inputs: the number of the channel of the slave, register address of the slave
// Return: None
{
	uint32_t I2CBASE = SelectChannel(channel);
    
    // Specify that we are writing (a register address) to the slave device
    I2CMasterSlaveAddrSet(I2CBASE, SLAVE_ADDR, false);
 
    // Specify register to be read
    I2CMasterDataPut(I2CBASE, reg);
 
    // Send control byte and register address byte to slave device
    I2CMasterControl(I2CBASE, I2C_MASTER_CMD_BURST_SEND_START);
     
    // Wait for MCU to finish transaction
    while(I2CMasterBusy(I2CBASE));
     
    // Specify that we are going to read from slave device
    I2CMasterSlaveAddrSet(I2CBASE, SLAVE_ADDR, true);
     
    // Send control byte and read from the register we specified
    I2CMasterControl(I2CBASE, I2C_MASTER_CMD_SINGLE_RECEIVE);
     
    // Wait for MCU to finish transaction
    while(I2CMasterBusy(I2CBASE));
     
    // Return data pulled from the specified register
    return I2CMasterDataGet(I2CBASE);
}


void I2CSend(uint8_t channel, uint8_t reg, uint8_t data)
//******************************************************
// Desc: Sends an I2C command to the specified slave
// Inputs: the number of the channel of the slave, the register address of the slave, data to be sent to the slave
// Return: None
{
	uint32_t I2CBASE = SelectChannel(channel);
    
    // Tell the master module what address it will place on the bus when communicating with the slave.
    I2CMasterSlaveAddrSet(I2CBASE, SLAVE_ADDR, false);
          
    // Put register address of the slave into FIFO
    I2CMasterDataPut(I2CBASE, reg);
     
	// Initiate send of data from the MCU
	I2CMasterControl(I2CBASE, I2C_MASTER_CMD_BURST_SEND_START);
	 
	// Wait until MCU is done transferring.
	while(I2CMasterBusy(I2CBASE))
	{
	}
	
    // Put data to be sent into FIFO
    I2CMasterDataPut(I2CBASE, data);
     
	// Initiate send of data from the MCU
	I2CMasterControl(I2CBASE, I2C_MASTER_CMD_BURST_RECEIVE_FINISH);

	// Wait until MCU is done transferring.
	while(I2CMasterBusy(I2CBASE))
	{
	}
}

uint32_t SelectChannel(unsigned char channel)
//*******************************************
// Desc: Selects the I2C Base that will be used for the I2C module
// Inputs: number of the channel to be used
// Return: I2C Base
{
	uint32_t I2CBASE;
	switch (channel)
	{
		case 0:
			I2CBASE = I2C0_BASE;
			break;
		case 1:
			I2CBASE = I2C2_BASE;
			break;
		case 2:
			I2CBASE = I2C3_BASE;
			break;
		case 3:
			I2CBASE = I2C4_BASE;
			break;
		
		default:
			break;
	}
		
	return I2CBASE;			
}

void init_i2c0(void)
//*****************
// Desc: Initialise the I2C0 Module 
// Inputs: None
// Return: None
{
    debugf(DEBUG_CLK, "Initialising I2C Module...\n");
    
    // Enable I2C module 0
    SysCtlPeripheralEnable(I2C_PERIPH); 
    
    // Reset module
    SysCtlPeripheralReset(I2C_PERIPH); 
    
    // Enable GPIO peripheral that contains I2C 0
    SysCtlPeripheralEnable(I2C_GPIO_PERIPH); 
    
    // Configure the pin muxing for I2C0 functions on port B2 and B3.
    GPIOPinConfigure(I2C_PINCON_CLK);
    GPIOPinConfigure(I2C_PINCON_DAT);
     
    // Select the I2C function for these pins.
    GPIOPinTypeI2CSCL(I2C_GPIO_BASE, I2C_PIN_CLK);
    GPIOPinTypeI2C(I2C_GPIO_BASE, I2C_PIN_DAT);
 
    // Enable and initialize the I2C0 master module.  Use the system clock for
    // the I2C0 module.  The last parameter sets the I2C data transfer rate.
    // If false the data rate is set to 100kbps and if true the data rate will
    // be set to 400kbps.
    I2CMasterInitExpClk(I2C_BASE, PROCESSOR_CLOCK, false);
    
    debugf(DEBUG_CLK, "I2C Module Initialised Successfully!\n");
}

void ReadStartupConfigChannels ( struct Si570_Params *Si570Channels, ChanNum channel )
//************************************************************************************
// Desc: Reads start-up register contents for RFREQ, HS_DIV, and N1 and calculates the internal crystal frequency (FXTAL)
// Inputs: parameters of the Si570 structure, number of the channel at which Si570 is connected
// Return: None
{
	unsigned int idx;
	
	// Power up and Enable the Si570 Module
    GPIOPinWrite(C1_OE_PORTBASE, C1_OE_PIN, PIN_HIGH);
	
	//Recall values into RAM from NVM. Write 0x01 into REG_135
	I2CSend( channel, REG_135, 0x01);
    
	//Read back the content of register 7 into sequential arrays
    for (idx = 0; idx < 6; idx++)
	{
		Si570Channels->reg[idx] = I2CReceive(channel, REG_7+idx);
	}
    
    // Get value fo INITIAL_HSDIV from REG[0]. 4 is added to this because the bits "000" correspond to an HSDIV of 4, 
    // so there is an offset of 4. See the register 7 of Si570 datasheet for more information.
	Si570Channels->def_HS_DIV = ((Si570Channels->reg[0] & 0xE0) >> 5) + 4; 
    
    // Get correct value of INITIAL_N1 by adding parts of REG[0] and REG[1]
    Si570Channels->def_N1 = (( Si570Channels->reg[0] & 0x1F ) << 2 ) + (( Si570Channels->reg[1] & 0xC0 ) >> 6 );
    
    if(Si570Channels->def_N1 == 0)
    {
        // This is a corner case of N1
        Si570Channels->def_N1 = 1;
    }
    else if((Si570Channels->def_N1 & 1) != 0)
    {
        // As per datasheet, illegal odd divider values should should be rounded up to the nearest even value.
        Si570Channels->def_N1 = Si570Channels->def_N1 + 1; 
    }	 
    
    // Read initial value for RFREQ. A 34-bit number is fit into a 32-bit space by ignoring lower 2 bits.
    Si570Channels->def_ulRFREQ = (Si570Channels->reg[1] & 0x3F );
    Si570Channels->def_ulRFREQ = (Si570Channels->def_ulRFREQ << 8) + ( Si570Channels->reg[2] );
    Si570Channels->def_ulRFREQ = (Si570Channels->def_ulRFREQ << 8) + ( Si570Channels->reg[3] );
    Si570Channels->def_ulRFREQ = (Si570Channels->def_ulRFREQ << 8) + ( Si570Channels->reg[4] );
    Si570Channels->def_ulRFREQ = (Si570Channels->def_ulRFREQ << 8) + ( Si570Channels->reg[5] );
    
    // Convert the RFREQ value 
	Si570Channels->def_RFREQ = (double)(Si570Channels->def_ulRFREQ) / POW_2_28;
    
    //Default Frequecy for Selected output
	FOUT0 = 100.00;
	Si570Channels->def_FOUT = FOUT0;
    
    // Crystal Frequency (FXTAL) calculation in MHz
    Si570Channels->FXTAL = (FOUT0 * Si570Channels->def_N1 * Si570Channels->def_HS_DIV) / Si570Channels->def_RFREQ;
    
	debugf(DEBUG_CLK, "Rg7:%d,Rg8:%d,Rg9:%d,Rg10:%d,Rg11:%d,Rg12:%d",
        Si570Channels->reg[0],Si570Channels->reg[1],Si570Channels->reg[2],
        Si570Channels->reg[3],Si570Channels->reg[4],Si570Channels->reg[5]);	
}

void ReadConfigRegsChannels ( struct Si570_Params *Si570Channels, ChanNum channel )
//*********************************************************************************
// Desc: Reads the configuration registers of the Si570
// Inputs: parameters of the Si570 structure, number of the channel at which Si570 is connected
// Return: None
{
	unsigned int idx;
	char dbgStr[50];
	
	// Power up and Enable the Si570 Module
    GPIOPinWrite(C1_OE_PORTBASE, C1_OE_PIN, PIN_HIGH);

	//Read back the content of register 7 into sequential arrays
    for (idx = 0; idx < 6; idx++)
	{
		Si570Channels->reg[idx] = I2CReceive(channel, REG_7+idx);
	}
    
    // Get value fo INITIAL_HSDIV from REG[0]. 4 is added to this because the bits "000" correspond to an HSDIV of 4, 
    // so there is an offset of 4. See the register 7 of Si570 datasheet for more information.
	Si570Channels->def_HS_DIV = ((Si570Channels->reg[0] & 0xE0) >> 5) + 4; 
    
    // Get correct value of INITIAL_N1 by adding parts of REG[0] and REG[1]
    Si570Channels->def_N1 = (( Si570Channels->reg[0] & 0x1F ) << 2 ) + (( Si570Channels->reg[1] & 0xC0 ) >> 6 );
    
    if(Si570Channels->def_N1 == 0)
    {
        // This is a corner case of N1
        Si570Channels->def_N1 = 1;
    }
    else if((Si570Channels->def_N1 & 1) != 0)
    {
        // As per datasheet, illegal odd divider values should should be rounded up to the nearest even value.
        Si570Channels->def_N1 = Si570Channels->def_N1 + 1;
    }	 

    // Read initial value for RFREQ. A 34-bit number is fit into a 32-bit space by ignoring lower 2 bits.
    Si570Channels->def_ulRFREQ = ( Si570Channels->reg[1] & 0x3F );
    Si570Channels->def_ulRFREQ = (Si570Channels->def_ulRFREQ << 8) + ( Si570Channels->reg[2] );
    Si570Channels->def_ulRFREQ = (Si570Channels->def_ulRFREQ << 8) + ( Si570Channels->reg[3] );
    Si570Channels->def_ulRFREQ = (Si570Channels->def_ulRFREQ << 8) + ( Si570Channels->reg[4] );
    Si570Channels->def_ulRFREQ = (Si570Channels->def_ulRFREQ << 8) + ( Si570Channels->reg[5] );
	 
    // Convert the RFREQ value 
	Si570Channels->def_RFREQ = (double)(Si570Channels->def_ulRFREQ) / POW_2_28;
		
    // Crystal Frequency (FXTAL) calculation
    Si570Channels->FXTAL = (double)(Si570Channels->FOUT * Si570Channels->def_N1 * Si570Channels->def_HS_DIV) / Si570Channels->def_RFREQ;          //MHz
	 
	sprintf(dbgStr,"Rg7:%2X,Rg:%2X,Rg9:%2X,Rg10:%2X,Rg11:%2X,Rg12:%2X, \n",Si570Channels->reg[0],Si570Channels->reg[1],Si570Channels->reg[2],Si570Channels->reg[3],Si570Channels->reg[4],Si570Channels->reg[5]);	 
	debugf(DEBUG_CLK, dbgStr);
    
//	 sprintf(dbgStr,"I-HS_DIV:%d,I-N1:%d,I-RFREQ:%llu,D-RFREQ:%f,FXTAL:%fMHz\n",Si570Channels->def_HS_DIV, Si570Channels->def_N1, Si570Channels->def_ulRFREQ, Si570Channels->def_RFREQ, Si570Channels->FXTAL);
//	 OutputDebugString(dbgStr);
}

void ConfigOutputFreqChannels( struct Si570_Params *Si570Channels, ChanNum channel )
//**********************************************************************************
// Desc: Programs the Si570 module on the selected frequency
// Inputs: parameters of the Si570 structure, number of the channel at which Si570 is connected
// Return: None
{
	
	float temp;
	int HSDIVN1;
	
	unsigned long ul_RFREQint = 0;
	unsigned long ul_RFREQfrac_part = 0;
	unsigned long long temp_RFREQ = 0;
	double ul_RFREQfrac = 0;
	
	int i = 0;
	unsigned char REG_FREEZEDCO;
	unsigned char REG_MEMCTL;
    
    // The following values for N1 and HS_DIV have been selected based on calculations and the software tool provided by Silicon Labs.
    // The Si570 has been configured in this FW to output frequencies in the range 6MHz to 20MHz. If greater frequencies needed in the
    // future, then more conditional statements should be added below to reflect the new range.
    if (6.00 <= Si570Channels->FOUT && Si570Channels->FOUT < 7.00)
    {
        Si570Channels->HS_DIV = HS_DIV[4];
        Si570Channels->N1 = 90;
    }
    else if (7.00 <= Si570Channels->FOUT && Si570Channels->FOUT < 8.00)
    {
        Si570Channels->HS_DIV = HS_DIV[2];
        Si570Channels->N1 = 116;
    }
    else if (8.00 <= Si570Channels->FOUT && Si570Channels->FOUT < 9.00)
    {
        Si570Channels->HS_DIV = HS_DIV[5];
        Si570Channels->N1 = 54;
    }
    else if (9.00 <= Si570Channels->FOUT && Si570Channels->FOUT < 10.00)
    {
        Si570Channels->HS_DIV = HS_DIV[5];
        Si570Channels->N1 = 52;
    }
    else if (10.00 <= Si570Channels->FOUT && Si570Channels->FOUT < 11.00)
    {
        Si570Channels->HS_DIV = HS_DIV[5];
        Si570Channels->N1 = 46;
    }
    else if (11.00 <= Si570Channels->FOUT && Si570Channels->FOUT < 12.00)
    {
        Si570Channels->HS_DIV = HS_DIV[4];
        Si570Channels->N1 = 50;
    }
    else if (12.00 <= Si570Channels->FOUT && Si570Channels->FOUT < 13.00)
    {
        Si570Channels->HS_DIV = HS_DIV[5];
        Si570Channels->N1 = 38;
    }
    else if (13.00 <= Si570Channels->FOUT && Si570Channels->FOUT < 15.00)
    {
        Si570Channels->HS_DIV = HS_DIV[5];
        Si570Channels->N1 = 34;
    }
    else if (15.00 <= Si570Channels->FOUT && Si570Channels->FOUT < 17.00)
    {
        Si570Channels->HS_DIV = HS_DIV[5];
        Si570Channels->N1 = 30;
    }
    else if (17.00 <= Si570Channels->FOUT && Si570Channels->FOUT < 19.00)
    {
        Si570Channels->HS_DIV = HS_DIV[5];
        Si570Channels->N1 = 26;
    }
    else if (19.00 <= Si570Channels->FOUT && Si570Channels->FOUT < 21.00)
    {
        Si570Channels->HS_DIV = HS_DIV[5];
        Si570Channels->N1 = 24;
    }
    else
    {
        Si570Channels->HS_DIV = HS_DIV[5];
        Si570Channels->N1 = 22;
    }
			
	HSDIVN1 = Si570Channels->HS_DIV * Si570Channels->N1 ;

    // New RFREQ calculation
    Si570Channels->RFREQ = (Si570Channels->FOUT * HSDIVN1) / Si570Channels->FXTAL;

    // Calculate RFREQ organizing the float variables to save precision;
    // RFREQ is kept as an unsigned long
    // only 32 bits are available in the long format
    // RFREQ in the device has 34 bits of precision
    // only 34 of the 38 bits are needed since RFREQ is between 42.0 and
    // 50.0 for fxtal of 114.285MHz (nominal)

	ul_RFREQint = Si570Channels->RFREQ;
	ul_RFREQfrac = Si570Channels->RFREQ - ul_RFREQint;
	ul_RFREQfrac_part = ul_RFREQfrac * (1L<<28);	
	
    // RFREQ long integer
	Si570Channels->ulRFREQ = temp * (1L<<28);

	//Si570Channels->ulRFREQ = ul_RFREQint;
	temp_RFREQ = ul_RFREQint;
	temp_RFREQ = (temp_RFREQ << 28) | ul_RFREQfrac_part;
    
	//update registers to be written back to crystal
	//sprintf(dbgStr2,"N-HS_DIV:%d,N-N1:%d, N-RFEEQ:%llu, D-RFREQ:%f, Alt-RFREQ:%llu, FXTAL:%f MHz,frac:%f,int:%lu \n",Si570Channels->HS_DIV, Si570Channels->N1, Si570Channels->ulRFREQ, Si570Channels->RFREQ, temp_RFREQ, Si570Channels->FXTAL, ul_RFREQfrac, ul_RFREQint);
	//OutputDebugString(dbgStr2);
	
    // Clear registers
	for(i = 0; i < SI570_REGLEN; i++)
    {
       Si570Channels->reg[i] = 0;
    }
    
    debugf(DEBUG_CLK, "Si570 holding registers flushed \n");
    debugf(DEBUG_CLK, "Rg7:%d, \n",Si570Channels->reg[0]);
    debugf(DEBUG_CLK, "Rg8:%d, \n",Si570Channels->reg[1]);
    debugf(DEBUG_CLK, "Rg9:%d, \n",Si570Channels->reg[2]);
    debugf(DEBUG_CLK, "Rg10:%d, \n",Si570Channels->reg[3]);
    debugf(DEBUG_CLK, "Rg11:%d, \n",Si570Channels->reg[4]);
    debugf(DEBUG_CLK, "Rg12:%d, \n",Si570Channels->reg[5]);
    
    // Subtract 4 because of the offset of HS_DIV. Ex: "000" maps to 4, "001" maps to 5
    Si570Channels->HS_DIV = Si570Channels->HS_DIV - 4;

    // Set the top 3 bits of REG[0] which will correspond to Register 7 on Si570
    Si570Channels->reg[0] = (Si570Channels->HS_DIV << 5);

    // Convert new N1 to the binary representation
    if(Si570Channels->N1 == 1)
    {
        //Corner case for N1. If N1=1, it is represented as "00000000"
        Si570Channels->N1 = 0;
    }
    else if((Si570Channels->N1 & 1) == 0)
    {
        // If n1 is even, round down to closest odd number. See the Si570 datasheet for more information.
        Si570Channels->N1 = Si570Channels->N1 - 1;
    }
		
    // Write correct new values to REG[0] through REG[6]
    // These will be sent to the Si570 and will update the output frequency
    Si570Channels->reg[0] = (Si570Channels->reg[0] & 0xE0) | (Si570Channels->N1 >> 2);          // Set N1 part of REG[0]
    Si570Channels->reg[1] = Si570Channels->N1 << 6;                                             // Set N1 part of REG[1]
        
    //Write new version of RFREQ to corresponding registers	 
    Si570Channels->reg[1] = Si570Channels->reg[1] | ((ul_RFREQint >> 4) & 0x3F) ;
    Si570Channels->reg[2] = ((ul_RFREQint& 0x0F ) << 4) | (ul_RFREQfrac_part >> 24);
    Si570Channels->reg[3] = ul_RFREQfrac_part >> 16;
    Si570Channels->reg[4] = ul_RFREQfrac_part >> 8;
    Si570Channels->reg[5] = ul_RFREQfrac_part;
 
    debugf(DEBUG_CLK, "Si570 holding registers loaded \n");
    debugf(DEBUG_CLK, "Rg7:%d, \n",Si570Channels->reg[0]);
    debugf(DEBUG_CLK, "Rg8:%d, \n",Si570Channels->reg[1]);
    debugf(DEBUG_CLK, "Rg9:%d, \n",Si570Channels->reg[2]);
    debugf(DEBUG_CLK, "Rg10:%d, \n",Si570Channels->reg[3]);
    debugf(DEBUG_CLK, "Rg11:%d, \n",Si570Channels->reg[4]);
    debugf(DEBUG_CLK, "Rg12:%d, \n",Si570Channels->reg[5]);
    
	// Read the current state of Register 137 
    REG_FREEZEDCO = I2CReceive(channel, REG_137);
    
    #ifdef UART_DEBUG
    OutputDebugString("Si570 Freeze DCO Register output \n");
    sprintf(dbgStr2,"Read DCO Reg:0x%2X \n",REG_FREEZEDCO);	 
    OutputDebugString(dbgStr2);
    #endif

    // Set the Freeze DCO bit in that register. This must be done in order to update. Registers 7-12 on the Si57x
    I2CSend(channel, REG_137, REG_FREEZEDCO|0x10);
    
    // Read the current state of Register 135
	REG_MEMCTL = I2CReceive(channel,REG_135);
    
	#ifdef UART_DEBUG
	OutputDebugString("Si570 Memory Ctl Register output \n");
	sprintf(dbgStr2,"Read Freeze M Bit:0x%2X \n",REG_MEMCTL);	 
	OutputDebugString(dbgStr2);
    #endif
    
    // Set the Freeze M bit to load the RFREQ registers
	I2CSend(channel,REG_135, REG_MEMCTL|0x20);
    
    // Read the current state of Register 137
	REG_MEMCTL = I2CReceive(channel,REG_135);
    
    #ifdef UART_DEBUG
	OutputDebugString("Si570 Memory Ctl Register output \n");
	sprintf(dbgStr2,"Set Freeze M Bit:0x%2X \n",REG_MEMCTL);	 
	OutputDebugString(dbgStr2);
    #endif

    // Write the new values to Registers 7-12
	for(i=0; i<SI570_REGLEN; i++)
    {
        I2CSend(channel,i+REG_7, Si570Channels->reg[i]);
    }
    
    // Clear the Freeze M bit to load the RFREQ registers
	I2CSend(channel,REG_135, REG_MEMCTL & 0x00);
    
    // Read the current state of Register 137
	REG_MEMCTL = I2CReceive(channel,REG_135);
    
    #ifdef UART_DEBUG
	OutputDebugString("Si570 Memory Ctl Register output \n");
	sprintf(dbgStr2,"Reset Freeze M Bit:0x%2X \n",REG_MEMCTL);	 
	OutputDebugString(dbgStr2);
    #endif
	 
    // Read the current state of Register 137
    REG_FREEZEDCO = I2CReceive(channel,REG_137);
    
    #ifdef UART_DEBUG
    OutputDebugString("Si570 Freeze DCO Register output \n");
    sprintf(dbgStr2,"Read DCO Bit:0x%2X \n",REG_FREEZEDCO);	 
    OutputDebugString(dbgStr2);
    #endif
	 
    // Clear the Freeze DCO bit
    I2CSend(channel,REG_137, REG_FREEZEDCO & 0x00);
    
    // Read the current state of Register 137
	REG_FREEZEDCO = I2CReceive(channel,REG_137);
    
    #ifdef UART_DEBUG
    OutputDebugString("Si570 Freeze DCO Register output \n");
    sprintf(dbgStr2,"Reset DCO Bit:0x%2X \n",REG_FREEZEDCO);	 
    OutputDebugString(dbgStr2);
    #endif
	 
    // Read the current state of Register 135
	REG_MEMCTL = I2CReceive(channel,REG_135);
    
    #ifdef UART_DEBUG
    OutputDebugString("Si570 Memory Ctl Register output \n");
    sprintf(dbgStr2,"DCO Reg:0x%2X \n",REG_MEMCTL);	 
    OutputDebugString(dbgStr2);
    #endif

    // Set the NewFreq bit to alert the DPSLL that a new frequency configuration	
	I2CSend(channel,REG_135, REG_MEMCTL|0x40);
    
    // Read the current state of Register 137
	REG_MEMCTL = I2CReceive(channel,REG_135);
    
    #ifdef UART_DEBUG
    OutputDebugString("Si570 Memory Ctl Register output \n");
    sprintf(dbgStr2,"Set New Freq Bit:0x%2X \n",REG_MEMCTL);	 
    OutputDebugString(dbgStr2);
    #endif
	 
	ReadConfigRegsChannels( Si570Channels, channel );
}

//****************************************************************************
//
// Local Functions
//
//****************************************************************************


