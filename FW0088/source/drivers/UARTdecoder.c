//*****************************************************************************
//
// Include
//
//*****************************************************************************

// message format {0xA5, 0x5A rest of payload}
// [ 0xA5,0X5A,<data length>,<tag ID>,<tag mode>,<packet count*>,<data>,<crc> ]

#include <stdint.h>

// Standard Library Includes

// TivaWare includes

// TivaWare driverlib

// LWIP

// FATFs

// Include the text replace for hardware definitions
#include "hardware.h"

// Include the Firmware details file

// Must be called before micro SD

// Instrumentel HW Layer
#include "hw_layer/inst_eeprom.h"
#include "hw_layer/inst_uart.h"

// Instrumentel Drivers
#include "drivers/UARTdecoder.h"
#include "drivers/Si570.h"


// Instrumentel Application
#include "application/main.h"


//****************************************************************************
//
// Prototypes
//
//****************************************************************************

static inline void CommandProcess( UARTDecoder * ud );

//****************************************************************************
//
// Global Variables
//
//****************************************************************************


//****************************************************************************
//
// Local Variables
//
//****************************************************************************


//****************************************************************************
//
// Global Functions
//
//****************************************************************************

//#define OutputDebugString(x) UART_TXString(x)
#define OutputDebugString(x)

void UARTDecoder_Init( UARTDecoder * ud , uint8_t * buff, uint8_t bufflen)
/************************************************************************/
{
    ud->call = UARTDECODER_NULL;
    ud->state = UARTDECODER_STATE_SYNCBYTEONE;
    ud->length = 0;
    ud->data = buff;
    ud->maxdatalen = bufflen;
    
}

void UARTDecoder_DecodeMessageBuff( UARTDecoder * ud , uint8_t * pBuf, uint32_t len )
/***********************************************************************************/
{
    uint32_t i;
    
    for(i=0; i<len; i++)
    {
        UARTDecoder_DecodeMessage(ud, pBuf[i]);
    }
}

//#define UART_DECODER_DEBUG

void UARTDecoder_DecodeMessage( UARTDecoder * ud , uint8_t newByte )
/******************************************************************/
{
	#ifdef UART_DEBUG
    OutputDebugString( (uint8_t *)"Decode\n" );
    #endif

    switch( ud->state )
    {
        case UARTDECODER_STATE_SYNCBYTEONE:
            
            //UARTSendFormat("newByte = %x\n",newByte);
            
            #ifdef UART_DECODER_DEBUG
            UARTSendFormat((uint8_t *)"S1\n");
            #endif
            if ( 0xA5 == newByte )
            {
                ud->state = UARTDECODER_STATE_SYNCBYTETWO;
            }
            #ifdef UART_DEBUG
            else OutputDebugString((uint8_t *)"failed\0");
            #endif
            break;
            
        case UARTDECODER_STATE_SYNCBYTETWO:
        
            //UARTSendFormat("newByte2 = %x\n",newByte);
        
            #ifdef UART_DECODER_DEBUG
            UARTSendFormat((uint8_t *)"SB2\n");
            #endif
            if (newByte == 0x5A)
            {	
                ud->state = UARTDECODER_STATE_PAYLOADSIZE; 
            }
            else
            {
                #ifdef UART_DEBUG
                OutputDebugString((uint8_t *)"Revert to SB1\0");
                #endif
                ud->state = UARTDECODER_STATE_SYNCBYTEONE;
            }
            break;
            
        case UARTDECODER_STATE_PAYLOADSIZE:
            
            // Save this message lengthsize for use in UART_DATA
            ud->length = newByte;
        
            // Initialise index into buffer for storing data bytes
            ud->dataindex = 0;
            
            // UARTSendFormat("Length: 0x %d\n", newByte);
            
            #ifdef UART_DECODER_DEBUG
            //sprintf( dbgStr, "Length: %d \n", gUART0PacketLength );
            UARTSendFormat( "Length: %X\n" , ud->length);
            //UART_TXformatHEX(ud->length);
            //OutputDebugString("\n");    
            #endif
        
            if(ud->length == 0)
            {
                CommandProcess(ud);
                ud->state = UARTDECODER_STATE_SYNCBYTEONE;
            }
            else
            {
                ud->state = UARTDECODER_STATE_UARTDATA;
            }
            break;
            
        case UARTDECODER_STATE_UARTDATA:
            // Save all the data into an array
            // Store the current byte into preset packet data array
        
            // Only copy data if it's within size
            if(ud->dataindex < ud->maxdatalen)
                ud->data[ud->dataindex] = newByte;
            
            //UARTSendFormat("newByte3 = %x\n",newByte);
            //UARTSendFormat("ud->dataindex = %d\n", ud->dataindex);
            //UARTSendFormat("ud->length = %d\n", ud->length);
            
            #ifdef UART_DEBUG
			// sprintf( dbgStr, "B %X at %u \n" , newByte, gUART0PacketDataIndex);
            //OutputStringUART0( dbgStr );
            OutputDebugString( "B: 0x" );
            //UART_TXformatHEX(newByte);
            OutputDebugString(" at 0x");
            //UART_TXformatHEX(ud->dataindex);
            OutputDebugString("\n");
            #endif
            
            if( ud->dataindex >= ud->length - 1)   // remove command byte from length
            {
                #ifdef UART_DECODER_DEBUG
                UARTSendFormat( (uint8_t *)"\nData Fin\n");
                #endif
                
                //prepare for start of next packet
                ud->state = UARTDECODER_STATE_SYNCBYTEONE;
                
                //decode the data
                CommandProcess(ud);  
            }
            else
            {
                #ifdef UART_DECODER_DEBUG
                UARTSendFormat((uint8_t *)"inc pkt\n");
                #endif
                //UARTSendFormat("Increment !\n");
                ud->dataindex += 1;
            }
            break;
            
        default:
            #ifdef UART_DEBUG
            OutputDebugString( (uint8_t *)"Undefined Message Type\n\0" );
            #endif
        
            // Reset state machine
            ud->state = UARTDECODER_STATE_SYNCBYTEONE;
            break;
    }
}


void UARTDecoder_SetMessageCall(UARTDecoder * ud, UARTDecoderCall call)
{
    ud->call = call;
}

//****************************************************************************
//
// Local Functions
//
//****************************************************************************

static inline void CommandProcess( UARTDecoder * ud )
/***************************************************/
{
    if (ud->call != UARTDECODER_NULL)
    {
        #ifdef UART_DEBUG
        OutputDebugString( (uint8_t *)"Call CmdProc\n" );
        #endif
        ud->call(ud->data, ud->length, ud);
    }
    else
    {
        #ifdef UART_DEBUG
            OutputDebugString( (uint8_t *)"Cmd Proc is null!\n" );
            #endif
        UARTSendFormat("No Command Process!\n");
    }
}

