//*****************************************************************************
//
// Include
//
//*****************************************************************************

// Standard Library Includes
#include <stdint.h>
#include <limits.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>		  
#include <ctype.h>
#include <stdbool.h>

// TivaWare includes


// TivaWare driverlib


// LWIP


// FATFs


// Include the text replace for hardware definitions


// Include the Firmware details file


// Must be called before micro SD


// Instrumentel HW Layer


// Instrumentel Drivers
#include "drivers/drvr_debug_sw.h"

// Instrumentel Application
#include "application/app_string_functions.h"



//****************************************************************************
//
// Prototypes
//
//****************************************************************************

void my_reverse(char *s);


//****************************************************************************
//
// Global Variables
//
//****************************************************************************


//****************************************************************************
//
// Local Variables
//
//****************************************************************************

const char hexDigits[] = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};


//****************************************************************************
//
// Global Functions
//
//****************************************************************************

void strn2(char *dest, int maxLen, char *in1, char *in2)
{
    strncpy(dest, in1, maxLen);
    strncat(dest, in2, maxLen);
}

void strn3(char *dest, int maxLen, char *in1, char *in2, char *in3)
{
    strncpy(dest, in1, maxLen);
    strncat(dest, in2, maxLen);
    strncat(dest, in3, maxLen);
}

void strn4(char *dest, int maxLen, char *in1, char *in2, char *in3, char *in4)
{
    strncpy(dest, in1, maxLen);
    strncat(dest, in2, maxLen);
    strncat(dest, in3, maxLen);
    strncat(dest, in4, maxLen);
}

void strn5(char *dest, int maxLen, char *in1, char *in2, char *in3, char *in4, char *in5)
{
    strncpy(dest, in1, maxLen);
    strncat(dest, in2, maxLen);
    strncat(dest, in3, maxLen);
    strncat(dest, in4, maxLen);
    strncat(dest, in5, maxLen);
}

void strn6(char *dest, int maxLen, char *in1, char *in2, char *in3, char *in4, char *in5, char *in6)
{
    strncpy(dest, in1, maxLen);
    strncat(dest, in2, maxLen);
    strncat(dest, in3, maxLen);
    strncat(dest, in4, maxLen);
    strncat(dest, in5, maxLen);
    strncat(dest, in6, maxLen);
}


char * Inst_htoa(char input)
{
    static char output[3];

    output[0] = hexDigits[input&0x0f];	
    output[1] = hexDigits[(input>>4)&0x0f];
    output[2] = '\0';

    return output;
}

char * Inst_uitoa(int in)
{
    static char s[10];
    static int i;
    i = 9;
    
    s[i] = '\0';
                
    if(in > 100000000)
    {
        return &s[i];
    }

    do 
    {   
        i--;    
        s[i] = in % 10 + '0';		
    } 
    while ((in /= 10) > 0); 

    return &s[i];
}
    
char * Inst_itoa(int in)
{
    static int i;
    static int n;
    static int sign;
    static char s[20];
    n = in;
    
    if ((sign = i) < 0)  /* record sign */
    {
        n = -n;          /* make n positive */
    }
    
    while(n > 0)
    {
        n = n/10;
        i++;

        if(i > 18)
        {
            s[0] = '\0';
            return s;
        }
    }
    
    n = in;
    if ((sign = n) < 0)  /* record sign */
    {
        n = -n;          /* make n positive */
    }

    i = 0;
    do 
    {       
        /* generate digits in reverse order */
        s[i++] = n % 10 + '0';   /* get next digit */
    } 
    while ((n /= 10) > 0);     /* delete it */
    if (sign < 0)
    {
        s[i++] = '-';
    }
    s[i] = '\0';
    my_reverse(s);
    return s;
} 



void my_reverse(char *s)
{
    static int c, i, j;

    for (i = 0, j = strlen(s)-1; i<j; i++, j--) {
        c = s[i];
        s[i] = s[j];
        s[j] = c;
    }
}

    
typedef enum
{
   SKIP_WHITE,
   GET_DIGITS,
   FINISHED
} getint_type;

typedef enum
{
   POSITIVE,
   NEGATIVE
} sign_type;


int Inst_atoi(const char *s)
{
   static int limit;// = INT_MAX / 10;
   static getint_type state;// = SKIP_WHITE;
   static sign_type sign;// = POSITIVE;
   static int value;// = 0;
   static int ch;

   limit = INT_MAX / 10;
   state = SKIP_WHITE;
   sign = POSITIVE;
   value = 0;

   if (s != NULL)
   {
      while (state != FINISHED)
      {
         ch = (unsigned char)*s++;

         switch (state)
         {                  
            case SKIP_WHITE:

               /* ignore leading white space */
               if (isspace(ch))
               {
                  ;  /* do nothing at all!   */
               }

               else if (ch == '-')
               {
                  state = GET_DIGITS;
                  sign = NEGATIVE;
               }
               else if (ch == '+')
               {
                  state = GET_DIGITS;
               }
               else if (isdigit(ch))
               {
                  state = GET_DIGITS;
                  value = ch - '0';
               }

               else
               {
                  state = FINISHED;
               }
               break;

            case GET_DIGITS:
               if (isdigit(ch))
               {
                  ch -= '0';

                  if ((value < limit) ||
                     ((INT_MAX - value * 10) >= ch))
                  {
                     value *= 10;
                     value += ch;
                  }
                  else
                  {
                     value = INT_MAX;
                     state = FINISHED;
                  }
               }

               else
               {
                  state = FINISHED;
               }
               break;

            case FINISHED:
            default:
               break;

         }     /* end of switch (state)   */
      }     /* end of while (state != FINISHED) */
   }     /* end of if (s != NULL)   */

   /* now see if there was a negative sign   */
   if (sign == NEGATIVE)
   {
      value *= -1;
   }

   return value;
}


//! Convert String HEX to unsigned long value
unsigned long Inst_Otherstring2hex (char *str)
{
    unsigned int i, j = 0;
            
    while (str && *str && isxdigit(*str)) 
    {
        i = *str++ - '0';
        if (9 < i)
            i -= 7;
        j <<= 4;
        j |= (i & 0x0f);
    }

    for(i=0;str[i]!='\0';i++)
    {
        str[i] = '\0';
    }

    return(j);
}



//! Instrumentel String to Unsigned Long Hex
unsigned long Inst_string2hex(char str[3])
{
    unsigned long x;
    x = Inst_char2hex(str[1]);
    x = x + (Inst_char2hex(str[0])*16);
    return x;					 
}


//! Neil String to Unsigned Long Hex
unsigned long Inst_char2hex (char hex)
{
    unsigned long x;
    x=0;
    if(hex == '1')
    {
        x += 1;	
    }
    else if(hex == '2')
    {
        x += 2;
    }
    else if(hex == '3')
    {
        x += 3;
    }
    else if(hex == '4')
    {
        x += 4;
    }
    else if(hex == '5')
    {
        x += 5;
    }
    else if(hex == '6')
    {
        x += 6;
    }
    else if(hex == '7')
    {
        x += 7;
    }
    else if(hex == '8')
    {
        x += 8;
    }
    else if(hex == '9')
    {
        x += 9;
    }
    else if(hex == 'A')
    {
        x += 10;
    }
    else if(hex == 'B')
    {
        x += 11;
    }
    else if(hex == 'C')
    {
        x += 12;
    } 
    else if(hex == 'D')
    {
        x += 13;
    }
    else if(hex == 'E')
    {
        x += 14;
    }
    else if(hex == 'F')
    {
        x += 15;
    }
    else
    {
        x += 0;
    }
    return x;
}



int Inst_StringToInt(char *string, int length)
{
    static int i, strLength,Number;
    //static char temp[20]; 
    //debugf(DEBUG_GENERALstring);
    strLength = strlen(string);
    i=0;
    Number=0;
    while(	(i<99) && (i<length) && (i<strLength)&&(string[i]!='\0')&&(string[i]!='\n')&&
            ((string[i]=='0')||(string[i]=='1')||(string[i]=='2')||(string[i]=='3')||(string[i]=='4')||(string[i]=='5')
            ||(string[i]=='6')||(string[i]=='7')||(string[i]=='8')||(string[i]=='9')))
    {
        Number *= 10;
        Number += (Inst_CharToInt(string[i]));
        //snprintf(temp,10,"%d",Number);
        //debugf(DEBUG_GENERALtemp);
        i++;
    }
/*		
    if (string[0] == '-')
    {
        Number *= -1;
    }		*/
    return Number;
}

double Inst_StringToDouble(char *string, int length)
{
    static int i, j, strLength, Intpart;
    double decpart,decpart1;
    bool _negative = false;
    
    //static char temp[20]; 
    strLength = strlen(string);
    i=0;
    decpart=0;
    Intpart=0;
    
    if(string[0]=='-')
    {
        _negative = true; //return 0;
        i++;
    }
    
    // Work out the Integer part
    while(	(i<99) && (i<length) && (i<strLength)&&(string[i]!='\0')&&(string[i]!='\n')&&
            ((string[i]=='0')||(string[i]=='1')||(string[i]=='2')||(string[i]=='3')||(string[i]=='4')||(string[i]=='5')
            ||(string[i]=='6')||(string[i]=='7')||(string[i]=='8')||(string[i]=='9')))
    {
        Intpart *= 10;
        Intpart += (Inst_CharToInt(string[i]));
        i++;
    }
    
    //if(string[i]=='.')
    //{
        i++;
        decpart = 0;
        decpart1 = 0;
        j=10;
        // Work out the decimal part
        while(	(i<99) && (i<length) && (i<strLength)&&(string[i]!='\0')&&(string[i]!='\n')&&
                ((string[i]=='0')||(string[i]=='1')||(string[i]=='2')||(string[i]=='3')||(string[i]=='4')||(string[i]=='5')
                ||(string[i]=='6')||(string[i]=='7')||(string[i]=='8')||(string[i]=='9')))
        {
            decpart1 = (Inst_CharToInt(string[i]));
            if(decpart1!=0)
            {
                decpart1 = (decpart1/j);
            }
            decpart += decpart1;
            j = j*10;
            i++;
        }
    //}
    decpart += Intpart;		
        
    if (_negative)
    {
        decpart = decpart * -1;
    }
    
    return decpart;
}


int Inst_CharToInt(char charater)
{
    switch(charater)
    {
        case('1'):
            return 1;
        case('2'):
            return 2;
        case('3'):
            return 3;
        case('4'):
            return 4;
        case('5'):
            return 5;
        case('6'):
            return 6;
        case('7'):
            return 7;
        case('8'):
            return 8;
        case('9'):
            return 9;
        default:
            return 0;
    }
}

char* Inst_Int2hexstring(unsigned long var)
{
    static char returnstring[2];
    memset(returnstring,0x00,2);
    switch(var)
    {
        case(1):
            returnstring[0] = '1';
            break;
        case(2):
            returnstring[0] = '2';
            break;
        case(3):
            returnstring[0] = '3';
            break;
        case(4):
            returnstring[0] = '4';
            break;
        case(5):
            returnstring[0] = '5';
            break;
        case(6):
            returnstring[0] = '6';
            break;
        case(7):
            returnstring[0] = '7';
            break;
        case(8):
            returnstring[0] = '8';
            break;
        case(9):
            returnstring[0] = '9';
            break;
        case(10):
            returnstring[0] = 'A';
            break;
        default:
            returnstring[0] = '0';
            break;
    }
    return returnstring; 
}

unsigned long Inst_Long2String(char *data, unsigned long Number, unsigned long length, unsigned long PaddingFlag)
{
    static unsigned long i,ArrayIndex, LargestDenominator, lConvert, lNumber,ZeroPadFlag;
    if((length>100)|(length<=0))
    {
        data[0] = 0x00;
        return 0;
    }

    LargestDenominator = 1;
    lNumber = Number;
    lConvert = 0;
    for(i=0;i<(length-1);i++)
    {
        LargestDenominator = LargestDenominator *10;
    }
    // If it is larger than the length permits, send back all X's
    if(Number>((LargestDenominator*10)-1))
    {
        for(i=0;i<length;i++)
        {
            data[i] = '0';
        }
        data[i] = 0x00;
        return length;
    }
    ArrayIndex = 0;
    ZeroPadFlag = PaddingFlag;
    // Start at maximum value and work back untill we have no tens, just units
    for(i=0;i<length;i++)
    {
        // Work out how many of thousdands, hundreds, tens unit etc
        lConvert = lNumber/LargestDenominator;
        // Have we had a number yet, if not dont save it in the string (no zero padding)
        if((ZeroPadFlag == 0)&(lConvert!=0))
        {
                ZeroPadFlag = 1;
        }
        if(ZeroPadFlag!=0)
        {
            // Save data
            data[ArrayIndex] = Inst_IntToChar(lConvert);
            ArrayIndex++;
        }
        //If it is zero before the number		
        lNumber = lNumber - (lConvert*LargestDenominator);
        // Work out which column is next
        LargestDenominator = LargestDenominator/10;	
    }
    // If we have 0, return a zero
    if(ZeroPadFlag==0)
    {
        data[ArrayIndex] = '0';
        ArrayIndex++;
    }
    data[ArrayIndex] = 0x00;
    return ArrayIndex;
}

unsigned long Inst_Double2String(char *data, double Number, unsigned long IntegerLength, unsigned long DecimalLength)
{
    static unsigned long i,ArrayIndex, lNumber,Denominator;
    static double dDecimal;

    // Convert the interger part, and return the integer string
    lNumber = (unsigned long)Number;
    ArrayIndex = Inst_Long2String(data,lNumber, IntegerLength, 0);
    dDecimal =  Number - (double)lNumber;
    data[ArrayIndex] = '.';
    ArrayIndex++;
    data[ArrayIndex] = 0x00;
    Denominator = 10;
    for(i=0;i<(DecimalLength-1);i++)
    {
        Denominator = Denominator *10;
    }
    // Make the part we want to convert into a long
    lNumber = (unsigned long)(dDecimal*Denominator);
    // Convert that and save it after the decimal place
    Inst_Long2String(&data[ArrayIndex],lNumber, DecimalLength, 1);
    return ArrayIndex;
}

char Inst_IntToChar(unsigned long number)
{
    switch(number)
    {
        case(1):
            return '1';
        case(2):
            return '2';
        case(3):
            return '3';
        case(4):
            return '4';
        case(5):
            return '5';
        case(6):
            return '6';
        case(7):
            return '7';
        case(8):
            return '8';
        case(9):
            return '9';
        default:
            return '0';
    }
}


//****************************************************************************
//
// Local Functions
//
//****************************************************************************


