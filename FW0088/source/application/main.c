//*****************************************************************************
//
// Include
//
//*****************************************************************************

// Standard Library Includes
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>

// TivaWare includes
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_ints.h"

// TivaWare driverlib
#include "driverlib/debug.h"
#include "driverlib/gpio.h"
#include "driverlib/rom.h"
#include "driverlib/sysctl.h"
#include "driverlib/systick.h"
#include "driverlib/uart.h"
#include "driverlib/adc.h"
#include "driverlib/interrupt.h"

// Include the text replace for hardware definitions
#include "hardware.h"

// Include the Firmware details file

// Must be called before micro SD

// Instrumentel HW Layer
#include "hw_layer/hw_adc.h"
#include "hw_layer/hw_gpio.h"
#include "hw_layer/hw_eeprom.h"
#include "hw_layer/hw_uart.h"

// Instrumentel Drivers
#include "drivers/drvr_debug_sw.h"
#include "drivers/drvr_uart_codec.h"
#include "drivers/drvr_pin_state_decoder.h"
#include "drivers/drvr_manchester_decoder.h"
#include "drivers/drvr_Si570.h"

// Instrumentel Application
#include "application/app_string_functions.h"


//****************************************************************************
//
// Prototypes
//
//****************************************************************************


//****************************************************************************
//
// Global Variables
//
//****************************************************************************

bool g_adc_dump;


//****************************************************************************
//
// Local Variables
//
//****************************************************************************


//****************************************************************************
//
// Global Functions
//
//****************************************************************************


//****************************************************************************
//
// Local Functions
//
//****************************************************************************


// System tick
void SysTickIntHandler( void )
//****************************
{
    static int32_t counter = 0;
    
    if (++counter > 1000)
    {
        //transmit = true;
        counter = 0;
        GPIOPinWrite(LED_BASE, LED_1, ~GPIOPinRead(LED_BASE, LED_1));
    }
}

//! \brief Interrupt Initilisation
void interrupt_init(void)
//************************
{   
    //IntPrioritySet(INT_UART0, 0x00);
    IntPrioritySet(INT_UART2, INT_PRIORITY_UART);
//    IntPrioritySet(INT_TIMER0A, 0x01);
//    IntPrioritySet(INT_TIMER1A, 0x01);
    
    /* Enable all processor interrupts. */
    IntMasterEnable();
    IntPriorityGroupingSet(4);	
}

// Initialises basic hardware with few supporting functions
void InitMiscHardware ( void )
//************************
{
    // Initisialise the LED pins that will be used to inidicate the RF Readings
	SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOL);
	GPIOPinTypeGPIOOutput(GPIO_PORTL_BASE, GPIO_PIN_0 );
    
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOL);
	GPIOPinTypeGPIOOutput(GPIO_PORTL_BASE, GPIO_PIN_1 );
    
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOL);
	GPIOPinTypeGPIOOutput(GPIO_PORTL_BASE, GPIO_PIN_2 );
    
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOL);
	GPIOPinTypeGPIOOutput(GPIO_PORTL_BASE, GPIO_PIN_3 );
    
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOL);
	GPIOPinTypeGPIOOutput(GPIO_PORTL_BASE, GPIO_PIN_4 );
    
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOL);
    GPIOPinTypeGPIOOutput(GPIO_PORTL_BASE, GPIO_PIN_5 );
}

void system_init(void)
//********************
// Desc: Setup the system clock speed, System tick interrupt and enable fault interrupts.
// Inputs: None
// Return: None
{   
    #if PCB_NUMBER == PCB_NUMBER_88
    
    // Configure clock source
	SysCtlClockSet( SYSCTL_SYSDIV_2_5  |    // system clock divider (2_5 = 80 MHz)
					SYSCTL_USE_PLL   |      // use PLL or External osc
					SYSCTL_XTAL_25MHZ |     // speed of external crystal
					SYSCTL_OSC_MAIN );      // use main/intern/hibernate oscillator
    
    #elif PCB_NUMBER == PCB_NUMBER_70
    
    // Configure clock source
	SysCtlClockSet( SYSCTL_SYSDIV_2_5  |    // system clock divider (2_5 = 80 MHz)
					SYSCTL_USE_PLL   |      // use PLL or External osc
					SYSCTL_XTAL_8MHZ |     // speed of external crystal
					SYSCTL_OSC_MAIN );      // use main/intern/hibernate oscillator
    
    #endif
                                                                                                         
    IntEnable(FAULT_PENDSV); 
    IntEnable(INT_SYSCTL);
    
    // Set up system tick 
	SysTickPeriodSet( PROCESSOR_CLOCK / 1000 );
	SysTickIntEnable();
	SysTickEnable();
}

//#define PRODUCTION
#define UART_DATA_INPUT                 0
#define RAW_ADC_OUTPUT                  0
#define TEST_PATTERN                    0

uint16_t adc_to_process[500];

int main( void )
//**************
{
    bool _dump = false;
    static uint8_t tag_data_buff[100];
    uint32_t pass = 0, fail = 0;
    
    // Disable all interrupts at start-up
    IntMasterDisable();
    
    // Initilse ARM System clock and system tick
    system_init();
    
    // Setup the LEDs
    setup_output_pin(LED_PERIPH, LED_BASE, LED_1);
    setup_output_pin(LED_PERIPH, LED_BASE, LED_2);
    setup_output_pin(LED_PERIPH, LED_BASE, LED_3);
    setup_output_pin(LED_PERIPH, LED_BASE, LED_4);
    setup_output_pin(LED_PERIPH, LED_BASE, LED_5);
    setup_output_pin(LED_PERIPH, LED_BASE, LED_6);
    
    // Setup the interrupt priorities
    interrupt_init();
    
    // Initilse Debug
    debug_init(DEBUG_GENERAL);
    debugf(DEBUG_GENERAL, "Starting...");
    
    // Initialise EEPROM module
	Init_EEPROM();
    
    // Initialise the ARM to ARM UART
    uarthw_init(ARM_TO_ARM_UART);
    
    // Initialise the ADC peripheral with DMA data transfer.
    init_adc();    
    init_dma();
    
    #if PCB_NUMBER == PCB_NUMBER_88
    
    // Setup the clock to handle the transmission of power and data
    init_i2c0();
    init_Si570();
    
    #endif
    
    #if UART_DATA_INPUT
    // Setup the decoder to handle test data from the Python Script
    UARTDecoder_Init(&g_UARTPythonDecoder, (uint8_t*)&python_decoder_buff, 100);
    
    #else
    
    // Turn on the ADC
    debugf(DEBUG_GENERAL, "Start acquistion.");
    StartAcquisition();
    
    #endif

    // Setup the encoder to transmit data to the daughterboard.
    UARTEncoder_Init(&g_UARTMoboEncoder);
    
    debugf(DEBUG_GENERAL, "Enter loop.");
    
	while(1)
	{            
        #if UART_DATA_INPUT
        // Decode UART data.
        if (g_UARTPythonDecoder.data_available)
        {
            // Get the ADC data from the string
            uint32_t _adc = Inst_StringToInt((char*)&g_UARTPythonDecoder.data[4], g_UARTPythonDecoder.length - 4);
            
            // Add it to the buffer
            g_sample_buff[g_sample_buff_in_index] = _adc;
            //debugf(DEBUG_GENERAL, "g_sample_buff[%d]: %d", g_sample_buff_in_index, g_sample_buff[g_sample_buff_in_index]);
            g_sample_buff_in_index += 1;
            //debugf(DEBUG_GENERAL, "g_sample_buff_in_index: %d", g_sample_buff_in_index);
            
            if (g_sample_buff_in_index == SAMPLE_BUFF_LEN)
            {
                g_sample_buff_in_index = 0;
            }

            // Clear the uart decoder for the next message
            memset(g_UARTPythonDecoder.data, 0x00, 100);
            g_UARTPythonDecoder.data_available = false;
        }
        #endif
        
        #if RAW_ADC_OUTPUT

        if (g_sample_buff_full)
        {
            g_sample_buff_full = false;

            StopAcquisition();
            
            for (int x = 0; x < SAMPLE_BUFF_LEN; x++)
            {
                debugf(DEBUG_GENERAL, "ADC[%d]: %d", x, g_sample_buff[x]);
            }
            
            g_sample_buff_in_index = 0;
            ADCSequenceEnable( ADC0_BASE, SEQ0 );
        }    
        
        #else
        
        // If we have a full buffer of data then decide what to do next 
        if (g_sample_buff_in_index != g_sample_buff_out_index)
        {
            GPIOPinWrite(LED_BASE, LED_4, 0);
            //debugf(DEBUG_GENERAL, "Sample buff[]: out_index: %d, in_index: %d", g_sample_buff_out_index, g_sample_buff_in_index);

            #if 1

            while((g_sample_buff_in_index != g_sample_buff_out_index))
            {
                //GPIOPinWrite(LED_BASE, LED_3, ~GPIOPinRead(LED_BASE, LED_3));
                //debugf(DEBUG_PIN_STATE, "g_sample_buff: %d", g_sample_buff[g_sample_buff_out_index]);
                check_pin_state(g_sample_buff[g_sample_buff_out_index]);

                g_sample_buff_out_index += 1;

                if (g_sample_buff_out_index == SAMPLE_BUFF_LEN)
                {
                    g_sample_buff_out_index = 0;
                }
            }

            #else

            memcpy(adc_to_process, (void*)&g_sample_buff[g_sample_buff_out_index], sizeof(adc_to_process));

            for (int x = 0; x < sizeof(adc_to_process)/2; x++)
            {
                //GPIOPinWrite(LED_BASE, LED_3, ~GPIOPinRead(LED_BASE, LED_3));
                //debugf(DEBUG_PIN_STATE, "g_sample_buff: %d", g_sample_buff[g_sample_buff_out_index]);
                check_pin_state(adc_to_process[x]);
            }

            g_sample_buff_out_index += 1000;

            if (g_sample_buff_out_index >= SAMPLE_BUFF_LEN)
            {
                g_sample_buff_out_index -= SAMPLE_BUFF_LEN;
            }

            #endif
            
           GPIOPinWrite(LED_BASE, LED_4, LED_4);
        }
        
        #if 0
        if (g_adc_dump)
        {
            g_adc_dump = false;
            
            StopAcquisition();
            uint32_t _adc_dump_index = g_sample_buff_in_index;
            
            for (int x = 0; x < (sizeof(g_sample_buff)/2); x++)
            {
                debugf(DEBUG_GENERAL, "ADC[%d]: %d", _adc_dump_index, g_sample_buff[_adc_dump_index]);
                
                _adc_dump_index += 1;
                
                if (_adc_dump_index == SAMPLE_BUFF_LEN)
                {
                    _adc_dump_index -= SAMPLE_BUFF_LEN;
                }
            }
            
            g_sample_buff_in_index = 0;
            ADCSequenceEnable( ADC0_BASE, SEQ0 );
        }
        #endif
        
        // We have some data so get ready to send it
        if (g_envelope_data_available)
        {
            g_envelope_data_available = false;
            
            #if 0
            debugf(DEBUG_GENERAL, "g_envelope_data_available");
            // encode_message((uint8_t*)&g_rx_buff, DATA_LEN + CS_LEN);
            
            UARTCharPut( ARM_TO_ARM_UART_BASE, 0xA5 );
            UARTCharPut( ARM_TO_ARM_UART_BASE, 0x5A );
            UARTCharPut( ARM_TO_ARM_UART_BASE, len+8 );
            UARTCharPut( ARM_TO_ARM_UART_BASE, 0x10 );          // type
            UARTCharPut( ARM_TO_ARM_UART_BASE, RFChan );        // Source
            UARTCharPut( ARM_TO_ARM_UART_BASE, 0x10 );          // Rx PIC
            UARTCharPut( ARM_TO_ARM_UART_BASE, _success);    // Valid data?
            UARTCharPut( ARM_TO_ARM_UART_BASE, 0 );             // Dummy
            UARTCharPut( ARM_TO_ARM_UART_BASE, 0 );             // Dummy
            UARTCharPut( ARM_TO_ARM_UART_BASE, 0 );             // Packet counter  - Leave blank
            UARTCharPut( ARM_TO_ARM_UART_BASE, RFChan );        // Tag ID - Leave blank
    
            for ( i = 0; i < len; i++)
            {	
                UARTCharPut( ARM_TO_ARM_UART_BASE, buff[i] );
                //UARTSendFormat("buff[%d]: %X\n", i, buff[i]);
            }
            
            #endif
            
            memset(tag_data_buff, 0x00, sizeof(tag_data_buff));
            
            tag_data_buff[0] = 0xA5;
            tag_data_buff[1] = 0x5A;
            tag_data_buff[2] = 25;
            tag_data_buff[3] = 0x10;
            tag_data_buff[4] = 1; // RF Channel
            tag_data_buff[5] = 0x10;
            tag_data_buff[6] = g_rx_buff[0];
            tag_data_buff[7] = 0;
            tag_data_buff[8] = 0;
            tag_data_buff[9] = 0;
            tag_data_buff[10] = 1;
            
            #if TEST_PATTERN
            
            // [0x00, 0x64, 0x00, 0xC8, 0x01, 0x90, 0x02, 0x58, 0x03, 0x20, 0x03, 0x84, 0x00, 0x00, 159, 195]
            
            tag_data_buff[11] = 0x00;
            tag_data_buff[12] = 0x00;
            tag_data_buff[13] = 0x64;
            tag_data_buff[14] = 0x00;
            tag_data_buff[15] = 0xC8;
            tag_data_buff[16] = 0x01;
            tag_data_buff[17] = 0x90;
            tag_data_buff[18] = 0x02;
            tag_data_buff[19] = 0x58;
            tag_data_buff[20] = 0x03;
            tag_data_buff[21] = 0x20;
            tag_data_buff[22] = 0x03;
            tag_data_buff[23] = 0x84;
            tag_data_buff[24] = 0x00;
            tag_data_buff[25] = 0x00;
            tag_data_buff[26] = 159;
            tag_data_buff[27] = 195;
                        
            
            #else
            for (int x = 0; x < 18; x++)
            {
                tag_data_buff[11+x] = g_rx_buff_holding[x];
                debugf(DEBUG_GENERAL, "tag_data_buff[%d]: %02X", 11+x, tag_data_buff[11+x]);
            }
            
            //debugf(DEBUG_GENERAL, "g_rx_buff[27]: %02X, g_rx_buff[28]: %02X", g_rx_buff[27], g_rx_buff[28]);
            
            #endif
            
            // Send the data
            for (int x=0; x < 29; x++)
            {
                
            }
            
            // For debugging
            for (uint32_t i = 0; i < 29; i++)
            {	
                UARTCharPut(ARM_TO_ARM_UART_BASE, tag_data_buff[i]);
                //debugf(DEBUG_GENERAL, "g_rx_buff[%d]: %X", i, tag_data_buff[i]);
            }
            
            if (g_rx_buff[0] == 0)
            {
                pass += 1;
                debugf(DEBUG_GENERAL, "Pass!");
            }
            else
            {
                fail += 1;
                debugf(DEBUG_GENERAL, "Fail: %X", g_rx_buff[0]);
            }
            
            memset((uint8_t*)&g_rx_buff, 0x00, sizeof(g_rx_buff));
        } 

        #endif
        
	}
}
