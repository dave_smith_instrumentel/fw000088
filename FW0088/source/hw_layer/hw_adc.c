//*****************************************************************************
//
// Include
//
//*****************************************************************************

// Standard Library Includes
#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include <stdio.h>

// TivaWare includes
#include "inc/hw_memmap.h"
#include "inc/hw_ints.h"
#include "inc/hw_adc.h"

// TivaWare driverlib
#include "driverlib/sysctl.h"
#include "driverlib/gpio.h"
#include "driverlib/interrupt.h"
#include "driverlib/adc.h"
#include "driverlib/udma.h"

// LWIP


// FATFs


// Include the text replace for hardware definitions
#include "hardware.h"


// Include the Firmware details file


// Must be called before micro SD


// Instrumentel HW Layer
#include "hw_layer/hw_adc.h"
//#include "inst_hibernate.h"
//#include "inst_timers.h"

// Instrumentel Drivers
#include "drivers/drvr_debug_sw.h"

// Instrumentel Application



#define DMA_TRANSFER_SIZE 1000

//****************************************************************************
//
// Prototypes
//
//****************************************************************************


//****************************************************************************
//
// Global Variables
//
//****************************************************************************


//****************************************************************************
//
// Local Variables
//
//****************************************************************************

typedef struct AnalogueMap {
    unsigned long number;
    unsigned long Periph;
    unsigned long Base;
    unsigned long Pin;
    unsigned long adc_ctrl;
} AnalogueMap; 


// NON-OPTO CHANNELS ONLY
static AnalogueMap allAnalogueMappings[] = 
{
    {
       1, 
       SYSCTL_PERIPH_GPIOE,
       GPIO_PORTE_BASE,
       GPIO_PIN_3,
       ADC_CTL_CH0
    },
    {
       2, 
       SYSCTL_PERIPH_GPIOE,
       GPIO_PORTE_BASE,
       GPIO_PIN_2,
       ADC_CTL_CH1        
    }
};

uint8_t pui8DMAControlTable[1024];

volatile    uint16_t     g_sampleBuffer[4096]; // big buffer of ADC data

// Source and destination buffers used for the DMA transfer.
uint16_t pingBuff[1524];
uint16_t pongBuff[1524];

uint8_t g_dmaBlockCounter = 0;

volatile uint16_t g_sample_buff[SAMPLE_BUFF_LEN + 200];
uint32_t g_sample_buff_in_index = 0;
uint32_t g_sample_buff_out_index = 0;
bool g_sample_buff_full;

//extern bool g_sampleBufferFullPostTrigger;
//extern volatile uint8_t g_sampleBuffer[TRUE_SAMPLEBUFFER_SIZE];

//****************************************************************************
//
// Global Functions
//
//****************************************************************************

void init_adc(void)
{
    debugf(DEBUG_ADC, "init_adc\n");
    
    SysCtlPeripheralEnable( SYSCTL_PERIPH_ADC0 );
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOE);
    GPIOPinTypeADC( GPIO_PORTE_BASE, GPIO_PIN_2 );
    
    //Disable ready for configuration
    ADCSequenceDisable( ADC0_BASE, SEQ0 );
    
    // Currently fix the sample rate at 500ksps due to voltage monitoring software limitations. 
    ADCClockConfigSet(ADC0_BASE, ADC_CLOCK_SRC_PLL | ADC_CLOCK_RATE_EIGHTH, 1);
    ADCReferenceSet(ADC0_BASE, ADC_REF_INT);
    
    ADCSequenceConfigure( ADC0_BASE, SEQ0, ADC_TRIGGER_ALWAYS, PRIORITY1 );  // ADC_TRIGGER_PROCESSOR, ADC_TRIGGER_TIMER, ADC_TRIGGER_ALWAYS
    
    //configure which analog channels to use per mode
    ADCSequenceStepConfigure( ADC0_BASE, SEQ0, STEP0, ADC_CTL_CH1 );
    ADCSequenceStepConfigure( ADC0_BASE, SEQ0, STEP1, ADC_CTL_CH1 );
    ADCSequenceStepConfigure( ADC0_BASE, SEQ0, STEP2, ADC_CTL_CH1 );
    ADCSequenceStepConfigure( ADC0_BASE, SEQ0, STEP3, ADC_CTL_CH1 );
    ADCSequenceStepConfigure( ADC0_BASE, SEQ0, STEP4, ADC_CTL_CH1 );
    ADCSequenceStepConfigure( ADC0_BASE, SEQ0, STEP5, ADC_CTL_CH1 );
    ADCSequenceStepConfigure( ADC0_BASE, SEQ0, STEP6, ADC_CTL_CH1 );
    ADCSequenceStepConfigure( ADC0_BASE, SEQ0, STEP7, ADC_CTL_CH1 | ADC_CTL_IE | ADC_CTL_END);
    
    //ADCHardwareOversampleConfigure(ADC0_BASE, 2);
    ADCSequenceEnable(ADC0_BASE, 0); //Once configuration is set, re-enable the sequencer
        
    return;
}

void init_dma (void )
/*************/
{
    debugf(DEBUG_ADC, "init_dma.\n");
    
    SysCtlPeripheralEnable( SYSCTL_PERIPH_UDMA);
    
	uDMAEnable(); // Enables uDMA
    
    // Set the base for the channel control table.
    uDMAControlBaseSet(&pui8DMAControlTable[0]);
    
    uDMAChannelAssign(UDMA_CH14_ADC0_0);
    
    // Configures the base address of the channel control table. Table resides in system memory and holds control
    //     information for each uDMA channel. Table must be aligned on a 1024-byte boundary. Base address must be
    //     configured before any of the channel functions can be used
    uDMAChannelAttributeDisable(UDMA_CHANNEL_ADC0, UDMA_ATTR_ALL);
    // Start with all

    uDMAChannelAttributeEnable(UDMA_CHANNEL_ADC0, UDMA_ATTR_USEBURST);
    // Only allow burst transfers
    
    uDMAChannelControlSet(UDMA_CHANNEL_ADC0 | UDMA_PRI_SELECT, UDMA_SIZE_16 | UDMA_SRC_INC_NONE | UDMA_DST_INC_16 | UDMA_ARB_4 | UDMA_NEXT_USEBURST);  // UDMA_ARB_4
    uDMAChannelControlSet(UDMA_CHANNEL_ADC0 | UDMA_ALT_SELECT, UDMA_SIZE_16 | UDMA_SRC_INC_NONE | UDMA_DST_INC_16 | UDMA_ARB_4 | UDMA_NEXT_USEBURST);  // UDMA_ARB_4
    // uDMAChannelControlSet(uint32_t ui32ChannelStructIndex, uint32_t ui32Control)
    // ui32Control is the logical OR of five values: the data size, the source address incremen, the destination address
    //     increment, the arbitration size and the burst flag

    uDMAChannelTransferSet(UDMA_CHANNEL_ADC0 | UDMA_PRI_SELECT, UDMA_MODE_PINGPONG, (void *)(ADC0_BASE + ADC_O_SSFIFO0), (void *)&pingBuff[0], DMA_TRANSFER_SIZE);
    uDMAChannelTransferSet(UDMA_CHANNEL_ADC0 | UDMA_ALT_SELECT, UDMA_MODE_PINGPONG, (void *)(ADC0_BASE + ADC_O_SSFIFO0), (void *)&pongBuff[0], DMA_TRANSFER_SIZE);
    // uDMAChannelTransferSet(uint32_t ui32ChannelStructIndex, uint32_t ui32Mode, void *pvSrcAddr, void *pvDstAddr, uint32_t ui32TransferSize)
    // pvSrcAddr is the source address for the transfer in this case from hw_adc.h ADC_O_SSFIFO0 is the result of the FIFO 0 register
    // pvDstAddr is the destination address for the transfer
    // ui32TransferSize is the number of data items to transfer
    
    uDMAChannelEnable(UDMA_CHANNEL_ADC0); // Enables DMA channel so it can perform transfers
}

void StartAcquisition ( void )
/****************************/
{       
    // this variable is part of the logic that stops triggers happening before buffer has valid data
    
    IntEnable(INT_ADC0SS0);
    ADCIntEnableEx(ADC0_BASE, ADC_INT_DMA_SS0); // Enables ADC interrupt source due to DMA on ADC sample sequence 0
}

// stop ADC conversions 
void StopAcquisition ( void )
/***************************/
{
    ADCSequenceDisable( ADC0_BASE, SEQ0 );
}

void ADCinterruptSequencer ( void )
/*********************************/
{   
    char _str[100];
    memset(_str, 0x00, 100);

    debugf(DEBUG_ADC, "ADCinterruptSequencer.");
    
    GPIOPinWrite(LED_BASE, LED_2, ~GPIOPinRead(LED_BASE, LED_2));
    
    //clear the interrupt
    ADCIntClear( ADC0_BASE, SEQ0 );
        
    // Handle the ping buff
    if (uDMAChannelModeGet(UDMA_CH14_ADC0_0 | UDMA_PRI_SELECT) == UDMA_MODE_STOP)
    {       
        
        memcpy((void*)&g_sample_buff[g_sample_buff_in_index], pingBuff, DMA_TRANSFER_SIZE*2);
        g_sample_buff_in_index += DMA_TRANSFER_SIZE;
        
        if (g_sample_buff_in_index == sizeof(g_sample_buff)/2)
        {
            g_sample_buff_full = true;
            g_sample_buff_in_index = 0;
        }
        
        debugf(DEBUG_ADC, "ping, memcpy g_sample_buff[%d, %d]", g_sample_buff_in_index, g_sample_buff_out_index);
        
        uDMAChannelTransferSet(UDMA_CHANNEL_ADC0 | UDMA_PRI_SELECT, UDMA_MODE_PINGPONG, (void *)(ADC0_BASE + ADC_O_SSFIFO0), (void *)&pingBuff, DMA_TRANSFER_SIZE);
    }
    
    // Handle the pong buff
    if (uDMAChannelModeGet(UDMA_CH14_ADC0_0 | UDMA_ALT_SELECT) == UDMA_MODE_STOP)
    {            
        memcpy((void*)&g_sample_buff[g_sample_buff_in_index], pongBuff, DMA_TRANSFER_SIZE*2);
        g_sample_buff_in_index += DMA_TRANSFER_SIZE; 

        if (g_sample_buff_in_index == SAMPLE_BUFF_LEN)
        {
            g_sample_buff_full = true;
            g_sample_buff_in_index = 0;
        }
        
        debugf(DEBUG_ADC, "pong, memcpy g_sample_buff[%d, %d]", g_sample_buff_in_index, g_sample_buff_out_index);

        uDMAChannelTransferSet(UDMA_CHANNEL_ADC0 | UDMA_ALT_SELECT, UDMA_MODE_PINGPONG, (void *)(ADC0_BASE + ADC_O_SSFIFO0), (void *)&pongBuff, DMA_TRANSFER_SIZE);
    }
    
    // Look at the 1000 samples we just collected and see if any of them cross the threshold to initiate a trigger event
    //ADC_Calculate();
    
    g_dmaBlockCounter++;
    
    #if 0
    // We've copied the data and interogated it, so look to see if we have hit the end of the buffer
    if (g_sampleBufWriteIdx >= SAMPLEBUFFER_SIZE) 
    {                
        g_sampleBufWriteIdx = 0;
        g_dmaBlockCounter = 0;
    }
    #endif
    
    //ADCSequenceEnable( ADC0_BASE, SEQ0 );
    
    //GPIOPinWrite( GPIO_PORTJ_BASE, LED_GREEN , 0x00 );
}


//****************************************************************************
//
// Local Functions
//
//****************************************************************************



