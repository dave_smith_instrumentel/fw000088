//*****************************************************************************
//
// Include
//
//*****************************************************************************

// Standard Library Includes
#include <stdint.h>
#include <stdbool.h>

// TivaWare includes


// TivaWare driverlib
#include "driverlib/gpio.h"
#include "driverlib/sysctl.h"

// Include the text replace for hardware definitions


// Include the Firmware details file


// Must be called before micro SD


// Instrumentel HW Layer
#include "hw_layer/hw_gpio.h"

// Instrumentel Drivers


// Instrumentel Application



//****************************************************************************
//
// Prototypes
//
//****************************************************************************


//****************************************************************************
//
// Global Variables
//
//****************************************************************************

GPIOState g_gpio;
SETUPState g_setup_state;


//****************************************************************************
//
// Local Variables
//
//****************************************************************************


//****************************************************************************
//
// Global Functions
//
//****************************************************************************

void setup_output_pin(uint32_t a_peripheral, uint32_t a_base, uint32_t a_pin)
//***************************************************************************
// Desc: Initialise a processor pin as a digital output
// Inputs: Peripheral, port and pin number
// Return: None
{
    SysCtlPeripheralEnable(a_peripheral);
    GPIOPinTypeGPIOOutput(a_base, a_pin );
}

void set_output_pin(uint32_t a_base, uint32_t a_pin, bool a_direction)
//********************************************************************
// Desc: Change the state of a digital pin
// Inputs: Port, pin number and desired direction
// Return: None
{    
    if (a_direction)
    {
        GPIOPinWrite(a_base, a_pin, a_pin);
    }
    else
    {
        GPIOPinWrite(a_base, a_pin, 0);
    }
}

void toggle_output_pin(uint32_t a_base, uint32_t a_pin)
//*****************************************************
// Desc: Toggle the state of an output pin.
// Inputs: Port and pin number
// Return: None
{
    GPIOPinWrite(a_base, a_pin, ~GPIOPinRead(a_base, a_pin));
}

void fw70_config_flag_init( void )
{
    g_gpio.is_config_loaded = false;
}

void fw70_set_config_loaded( void )
{
    g_gpio.is_config_loaded = true;
}

bool fw70_is_config_loaded( void )
{
    return g_gpio.is_config_loaded;
}

void fw70_setup_flag_init( void )
{
    g_setup_state.is_setup_finished = false;
}

void fw70_set_setup_finish_flag( void )
{
    g_setup_state.is_setup_finished = true;
}

bool fw70_is_setup_finished( void )
{
    return g_setup_state.is_setup_finished;
}


//****************************************************************************
//
// Local Functions
//
//****************************************************************************





