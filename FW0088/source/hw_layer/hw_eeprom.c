//*****************************************************************************
//
// Include
//
//*****************************************************************************

// Standard Library Includes
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

// TivaWare includes


// TivaWare driverlib
#include "driverlib/eeprom.h"
#include "driverlib/sysctl.h"

// LWIP


// FATFs


// Include the text replace for hardware definitions


// Include the Firmware details file
#include "hardware.h"

// Must be called before micro SD


// Instrumentel HW Layer
#include "hw_layer/hw_eeprom.h"

// Instrumentel Drivers
#include "drivers/drvr_debug_sw.h"

// Instrumentel Application



//****************************************************************************
//
// Prototypes
//
//****************************************************************************


//****************************************************************************
//
// Global Variables
//
//****************************************************************************


//****************************************************************************
//
// Local Variables
//
//****************************************************************************

uint32_t ChannelAddress[5] = {CH1_FREQADDRESS, CH2_FREQADDRESS, CH3_FREQADDRESS, CH4_FREQADDRESS, CH5_FREQADDRESS};


//****************************************************************************
//
// Global Functions
//
//****************************************************************************

void WriteFreqEEPROM (uint32_t frequency, uint32_t channel)
//********************
// Desc: Writes the frequency value into EEPROM.
// Inputs: frequency to be written, channel is the address in the EEPROM where the freqeuncy will be stored
// Return: None
{
	uint32_t puiData[1];
    
	//Erase location first
	puiData[0] = frequency;
	EEPROMProgram(puiData, ChannelAddress[channel], 4);
}

uint32_t ReadFreqEEPROM (uint32_t channel)
//********************
// Desc: Reads a valued from specific address of the EEPROM.
// Inputs: address of the EEPROM
// Return: value stored on that specific address
{
	uint32_t puiRead[1];

	//Call read function
	EEPROMRead(puiRead, ChannelAddress[channel], 4);
	return puiRead[0];
}

void Init_EEPROM (void)
//********************
// Desc: Initialise the EEPROM module.
// Inputs: None
// Return: None
{
    debugf(DEBUG_EEPROM, "Initialising EEPROM Module.");
    
	SysCtlPeripheralEnable(SYSCTL_PERIPH_EEPROM0);
	EEPROMInit();
    
    debugf(DEBUG_EEPROM, "EEPROM Module Initialised Succesfully!");
}

//****************************************************************************
//
// Local Functions
//
//****************************************************************************



